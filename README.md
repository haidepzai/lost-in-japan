# Hanabi Mitai - Nippon Life

## Introduction

> A Japanese based text adventure where you play a guy who will stay in Japan for a week as an exchange student. Your decision will change the output of the story!

## Detailed Function

>A romance Visual Novel where you have the option to date a girl.
>Choose wisely!
>This game is only for Android smartphone and is made with Android Studio.
>Programming language: Java

## How to use

> Install the apk on your smartphone

## Screenshot

| <a href="https://gitlab.com/haidepzai/lost-in-japan" target="_blank">**Hanabi Mitai - Nippon Life**</a>


| [![Hai](https://i.ibb.co/kqwXcMG/Hanabi.jpg)](https://gitlab.com/haidepzai/lost-in-japan)

| [![Hai](https://i.ibb.co/VwYjwgZ/Date.jpg)](https://gitlab.com/haidepzai/lost-in-japan)

