package com.example.japan;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.example.japan.Helper.BaseActivity;
import com.example.japan.Helper.MusicPlayer;

public class OkonomiyakiActivity extends BaseActivity {

    private TextSwitcher textSwitcher; //Like TextView but with Fade in - Fade out for TextView
    private ImageButton nextButton;

    private int stringIndex = 0;
    private String[] dialog = {
            "",
            "You: (I feel kinda bad for Miki but I already have promised Aiko to meet her today!)",
            "You: (Okay, so this must the place where we supposed to meet)",
            "You: (It's a really lively place, so many people and the scenery with the river here is just breathtaking!)",
            "You: (Alright, then let's go inside)",
            "Aiko: Ahhh hey " + LoginActivity.username + " how are you?",
            "You: Hey Aiko! Oh you are already here! Sorry for the wait!! I am doing fine and you?",
            "Aiko: Oh no, I also just arrived here a couple of minute before! So how was your fist day in school?",
            "You: Awesome! I already met a new friend! The people here are so nice!!",
            "Aiko: Cool! I'm glad you have great time!",
            "Aiko: I bet you must be hungry, so I've already ordered for us 2 Okonomiyaki, is it ok?",
            "You: Yeah of course, thanks!",
            "Aiko: So what's your plan here in Japan?",
            "You: Mhh that's a good question but I wanna see a lot of things like Asakusa, Shinjuku, Akihabara and Shibuya!",
            "Aiko: Oh that's a great idea! How about we go to Akihabara tomorrow?",
            "You: Sounds great!!",
            "Aiko: Oh our food is coming! Yay ♥",
            "You: Wooooooooow!", //Case 17
            "You: Let's eaaaaat!",
            "Both: ITADAKIMASU",
            "You: IT'S SUPER!",
            "Aiko: Yay!",
            "You: Thanks always for showing me nice place to eat!",
            "Aiko: You are very welcome!", //Case 23
            "You: The Okonomiyaki was super duper delicious! It was a good choice to come here",
            "Aiko: Yessss I think so too!!",
            "You: It was a really nice evening with you Aiko!",
            "Aiko: Yeah! I feel the same... Hey " + LoginActivity.username + " what do you think if we go to Akihabara now?", //Case 27
            "You: Oh yes please! I wanna go there now! Let's go!!",
            "Aiko: Yay! Let's go ♥"

    };

    private TextView textView;
    private ImageView aikoView;
    private ImageView okonomiView;
    private ImageView moodView;

    private Button button1;
    private Button button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_okonomiyaki);

        button1 = findViewById(R.id.button);
        button2 = findViewById(R.id.button2);

        aikoView = findViewById(R.id.aikoFork);
        okonomiView = findViewById(R.id.imageOko);
        moodView = findViewById(R.id.imageMood);

        textSwitcher = findViewById(R.id.textSwitcher);
        nextButton = findViewById(R.id.imageButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){

                if (stringIndex == dialog.length-1){
                    playboy.stop();
                    Intent intent = new Intent(OkonomiyakiActivity.this, AkihabaraNightActivity.class);// New activity
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish(); // Call once you redirect to another activity
                } else {
                    textSwitcher.setText(dialog[++stringIndex]); //Else next Text from String
                }
                screenPopup();
            }
        });
        //Fade in - Fade out for text in the textView
        textSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                textView = new TextView(OkonomiyakiActivity.this); //Create new TextView with the settings below
                textView.setTextColor(Color.WHITE);
                textView.setTextSize(15);
                textView.setPadding(50,20,20,20);
                return textView;
            }
        });
        textSwitcher.setText(dialog[stringIndex]); //Start Text
        //Music
        playboy.play(this, R.raw.persona);
        playboy.loop(true);
        //Go to Akihabara
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                stringIndex = 28;
                textSwitcher.setText(dialog[stringIndex]);
                button1.setVisibility(View.INVISIBLE);
                button2.setVisibility(View.INVISIBLE);
                RoomActivity.akihabaraAiko = true;
            }
        });
        //Go Home
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playboy.stop();
                RoomActivity.okonomiyakiAiko = true;
                Intent intent = new Intent(OkonomiyakiActivity.this, RoomActivity.class);// New activity
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish(); // Call once you redirect to another activity
            }
        });
    }

    public void screenPopup(){
        switch(stringIndex){
            case 5:
                aikoView.animate().alpha(1).setDuration(1000);
                break;
            case 17:
                okonomiView.animate().alpha(1).setDuration(1000);
                break;
            case 20:
                okonomiView.animate().alpha(0).setDuration(1000);
                break;
            case 23:
            case 29:
                aikoView.setImageResource(R.drawable.aiko_food_blush);
                moodView.setVisibility(View.VISIBLE);
                break;
            case 25:
                moodView.setVisibility(View.INVISIBLE);
                aikoView.setImageResource(R.drawable.aiko_food);
                break;
            case 27:
                button1.setVisibility(View.VISIBLE);
                button2.setVisibility(View.VISIBLE);
                nextButton.setVisibility(View.INVISIBLE);
                break;
        }
    }
}
