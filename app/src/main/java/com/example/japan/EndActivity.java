package com.example.japan;

import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

import com.example.japan.Helper.BaseActivity;

public class EndActivity extends BaseActivity {

    final Handler handler = new Handler(); //Handler for delayed execute

    private ImageView endView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end);

        endView = findViewById(R.id.endView);

        playboy.play(EndActivity.this, R.raw.intro);
        endView.animate().alpha(1).setDuration(6000);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
                System.exit(0);
            }
        }, 15000);



    }
}
