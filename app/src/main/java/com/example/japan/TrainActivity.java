package com.example.japan;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.example.japan.Helper.BaseActivity;

public class TrainActivity extends BaseActivity {

    private TextSwitcher textSwitcher; //Like TextView but with Fade in - Fade out for TextView
    private ImageButton nextButton;
    private TextView textView;

    private ImageView shinjukuView;
    private ImageView guyView;
    private ImageView moodView;

    private int stringIndex = 0;
    private String[] dialog = {
            "On the train",
            "You: (Tonight it's the Matsuri)",
            "You: (I couldn't sleep well last night again but I'm looking forward to tonight)",
            "You: (It's near Shinjuku, so I will head to Shinjuku now and spend a bit time for myself first)",
            "You: (Japanese trains are so comfortable, quiet and clean... and over the top they rarely come late)",
            "You: (It's so convenient to travel through Tokyo by train)",
            "...",
            "Train announcement: まもなく 新宿 新宿 お出口は 右側です JR線 京王線 小田急線 都営大江戸線は お乗り換えです " +
                    "この電車は 新宿止まりです",
            "Train announcement: お忘れ物なさいませんようご注意下さい 都営地下鉄をご利用下さいまして ありがとうございました",
            "Train announcement: The next station is: Shinjuku, the door on the right side will open.",
            "Train announcement: Please change here for the Yamanote-Line, the Chuo-Line, the Odakyu-Line, the Keio-Line and the Shinjuku Subway Line",
            "You: (Alright)",
            "Shinjuku South Exit", //Case 12
            "You: (This station is so big omg \uD83D\uDE31 )",
            "You: (You can easily get lost here.. it has literally 100 exits...)",
            "You: (All these skyscrapper and people.. Big city life...)",
            "You: (I'm gonna miss Japan.. Definitely will come back here again!)",
            "You: mhhhh?",
            "Takuya: ....", //Case 18
            "You: You again...", //Index 19
            //Dialog when date Miki:
            "Takuya: I've heard what you did... you little brat!!",
            "You: ...",
            "Takuya: How dare you to made her upset!! I'm so angry at you.. I told you I will come for you!",
            "You: I have my reasons...",
            "Takuya: Your reason? You are nothing more than an egoist!",
            "You: You don't even know!! Shut up!!!",
            "Takuya: Don't you dare to meet Aiko again, I will take care of her!",
            "Takuya: Have a nice life!",
            "You: Shit.",
            "You: Gotta forget that... have to meet Miki soon!"
    };

    private int stringIndex2 = 0;
    private String[] dialog2 = {
            "Takuya: Yo... So you will spend time with Aiko today, right?",
            "You: Yeah.. have a problem?",
            "Takuya: Not at all..",
            "Takuya: Good luck man...",
            "You: Uhm... thanks dude!",
            "Takuya: See ya!",
            "You: Alright, time to see Aiko"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_train);

        shinjukuView = findViewById(R.id.shinjukuView);
        guyView = findViewById(R.id.imageGuy);
        moodView = findViewById(R.id.imageMood);

        textSwitcher = findViewById(R.id.textSwitcher);
        nextButton = findViewById(R.id.imageButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (stringIndex == 19 && School3Activity.dateAiko) {
                    textSwitcher.setText(dialog2[0]);
                    dialog = dialog2;
                    stringIndex = stringIndex2;
                    stringIndex2 = 5;
                } else if (stringIndex == dialog.length - 1) {
                    if(School3Activity.dateAiko){
                        playboy.stop();
                        Intent intent = new Intent(TrainActivity.this, AikoActivity.class);// New activity
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish(); // Call once you redirect to another activity
                    } else if (School3Activity.dateMiki){
                        playboy.stop();
                        Intent intent = new Intent(TrainActivity.this, MikiActivity.class);// New activity
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish(); // Call once you redirect to another activity
                    }
                } else {
                    textSwitcher.setText(dialog[++stringIndex]); //Else next Text from String
                }
                screenPopup();
            }
        });
        //Fade in - Fade out for text in the textView
        textSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                textView = new TextView(TrainActivity.this); //Create new TextView with the settings below
                textView.setTextColor(Color.WHITE);
                textView.setTextSize(15);
                textView.setPadding(50, 20, 20, 20);
                return textView;
            }
        });
        textSwitcher.setText(dialog[stringIndex]); //Start Text
        //Music
        playboy.play(this, R.raw.train);
        playboy.loop(true);

    }

    public void screenPopup() {

        switch (stringIndex) {
            case 5:
                if(stringIndex2 == 5){
                    guyView.animate().translationX(1300).setDuration(1000);
                }
                break;
            case 12:
                playboy.stop();
                shinjukuView.animate().alpha(1).setDuration(1000);
                playboy.play(TrainActivity.this, R.raw.afternoon);
                playboy.loop(true);
                break;
            case 18:
                guyView.animate().translationX(0).setDuration(1000);
                break;
            case 20:
                moodView.setVisibility(View.VISIBLE);
                break;
            case 28:
                moodView.setVisibility(View.INVISIBLE);
                guyView.animate().translationX(1300).setDuration(1000);
                break;
        }

    }

}
