package com.example.japan;

import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.example.japan.Helper.BaseActivity;
import com.example.japan.Helper.MusicPlayer;

public class TokyoActivity extends BaseActivity {
    private ConstraintLayout layout; //For changing background image later
    //private View decorView;

    private MusicPlayer wineSound = new MusicPlayer();

    private TextSwitcher textSwitcher; //Like TextView but with Fade in - Fade out for TextView
    private ImageButton nextButton;

    private static boolean likeAiko = true;

    private int stringIndex = 0;
    private String[] dialog = {
            "Tokyo - Near Main Station",
            "You: (That was a good walk to clear up my mind...)",
            "You: (What an awesome weather today!!)",
            "You: (This city never cease to amaze me.. so many people and everyone behaves so nice!)",
            "You: (The Yaesu Underground Shopping Street beneath the station was also really great)",
            "You: (I wish I could live here longer...)",
            "You: (Totally in love with Japan.. the people, the food, the culture.. all are so wonderful)",
            "You: (I should definitely improve my Japanese and come back again!)",
            "You: (There is so much to see, how about I take a day trip to Kamakura or to the sea in Enoden)",
            "You: (Or Tokyo Disney with Aiko or Miki maybe \uD83D\uDE0D )",
            "You: (Thanks to them I could eat and experience a lot...)",
            "You: (I have a feeling that both of them kinda like me...)",
            "You: (Oh time flies.. it's getting dark already)", //case 12
            "You: (Tokyo at night is also just beautiful like at daytime!)",
            "You: (Still plenty of people on the streets.. this city truly never sleeps...)",
            "You: I better get going.. I have a date now!",
            "Takuya: ......", //Case 16
            "You: It's you... Why are you stalking me?!",
            "Takuya: Calm down, it's just a coincidence",
            "You: So.. here for discussion again?",
            "Takuya: Hmpf...",
            "You: What's your damn problem?",
            "Takuya: I don't have any problem anymore. It's all fine.",
            "You: Well then, leave me alone?",
            "Takuya: Let me tell you something... If you ever break Aiko's heart, I will find you!!",
            "You: We are not even a couple!!",
            "Takuya: She seems to like you, are you stupid?",
            "You: ....",
            "Takuya: She was my number one... I did everything for her but it was never enough!!",
            "Takuya: I was obsessed and eventually she left me but that will never change my feeling to her..",
            "You: Dude... it's over you should move on!",
            "Takuya: What the hell do you know?! I just can't believe she hosts a guy like you..",
            "You: Well it's her decision and you should respect it!",
            "Takuya: SHE IS MINE!",
            "You: You treat her like an object! Do you even realize what you are talking about?",
            "Takuya: ...",
            "Takuya: Dammit.. I still can't get over it, she was all for me but I was nothing to her...",
            "You: Maybe you should reflect yourself and think again why she left you!",
            "Takuya: Do you really think I am a bad guy?", //Case 38
            //Not bad guy:
            "Takuya: Hmmm do you think so?",
            "You: Yeah I can understand you... maybe I would have react similar in that situation",
            "Takuya: ...",
            "You: Life goes on bro",
            "Takuya: Hummmm.. anyways, I gotta go! See ya!",
            //Continue:
            "You: what a weird guy", //Case 44
            "You: Better go home to see after Aiko...",
            "Aiko: Heyyyyyyyy " + LoginActivity.username + "!!!!!",
            "You: !!!?",
            "Aiko: Oh what a coincidence!", //Case 48
            "You: Wow hey Aiko! What are you doing here?",
            "Aiko: Just a little night walk to feel refreshed, how about you?",
            "You: Oh I just wanted to go home to see you...",
            "Aiko: To see me?", //Case 52
            "You: Yeah I was worried... and... you look beautiful today in this outfit!",
            "Aiko: !!! T-thanks... ♥", //Case 54
            "Aiko: I.. just wanted to ask you if you wanna go to Roppongi with me now?",
            "You: Roppongi? Now? It's famous for night life and clubs, no?",
            "Aiko: Yes! I thought we could go to the Ritz Carlton Hotel and see the the night view of Tokyo..",
            "You: R-ritz Carlton Hotel? It's a 5 star hotel!! \uD83D\uDE28",
            "Aiko: Yeah but I think you should see that view!!!",
            "You: O-okay! Let's go!!!",
            "Aiko: Yay!", //Case 61
            "Ritz Carlton Hotel in Roppongi",
            "You: Unbelievable....",
            "Aiko: Yeah....",
            "You: I can't believe we are here haha",
            "Aiko: Thanks for coming with me! What a view, right?",
            "You: Yesssss beautiful night view of Tokyo...",
            "Aiko: Let's drink wine?",
            "You: Sure!",
            "Waiter: Good evening! Have you already decided?", //Case 70
            "You: Hello, two glasses of wine, please",
            "Waiter: Understood.",
            "You: Thanks for showing me this view, I will never forget this!!",
            "Aiko: It's kinda romantic, isn't it?",
            "You: It is!",
            "Waiter: Here is your wine, sir!", //Case 76
            "You: Thank you so much",
            "You: This smells good and looks so clear! Wow!! Premium wine I guess",
            "Aiko & You: Cheers!!", //Case 79
            "You: What a wonderful night!",
            "Aiko: Yeah!! " + LoginActivity.username + ", you are really a good guy, thanks for everything",
            "Aiko: I really like you a lot ♥", //Case 82
            //If player likes her
            "Aiko: Ahhhhh... I feel embarrassed haha",
            "You: Hahahaha you are really special!!",
            "Aiko: Yeah you too! Thanks for the awesome night!",
            "You: No no I have to thank YOU! What is better than to drink wine on top of a 5 star hotel?",
            "Aiko: Hahaha I'm flattered... thanks " + LoginActivity.username + "...",
            "Aiko: By the way, the day after tomorrow is a big festival which is called お祭り (O-matsuri)\n" +
                    "I would be happy if you can join me! ♥",
            "You: Ohhh I always wanted to attend such festival and see firework! Sounds great!",
            "Aiko: Alright let's go back home soon!",
            "You: Yup! ありがとう (Thanks)",
            "Aiko: Haha nice.. okay let's go!"
    };
    //Bad guy:
    private boolean isBadGuy = false;
    private int stringIndex2 = -1;
    private String[] dialog2 = {
            "Takuya: hah... And you think you are better, Mr. perfect?",
            "Takuya: You are just a nothing!",
            "Takuya: I'm done with you. Have a nice life!"
    };

    //If player chose to go with Miki to Izakaya:
    private int stringIndex3 = -1;
    private String[] dialog3 = {
            "You: Alright, guess I have to go to see Miki now!",
            "You: Guess she is waiting for me already too long!!",
            "You: Better get going now!"
    };
    //Friendzone:
    private int stringIndex4 = -1;
    private String[] dialog4 = {
            "Aiko: Thanks.. I hope we can be good friends! Let's hang out a lot!",
            "You: Yup! It's always fun with you",
            "Aiko: I think so too! Let's keep in touch if you go back to your country",
            "You: Definitely!!",
            "Aiko: Wish we could have more time... Let's go soon!",
            "You: Yup! It was a good night, thanks Aiko!",
            "Aiko: You are welcome! So let's go!"
    };

    private TextView textView;
    private ImageView imageView;
    private ImageView sceneryView;
    private ImageView guyView;
    private ImageView moodView;
    private ImageView wineView;

    private Button button;
    private Button button2;
    private Button button3;
    private Button button4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tokyo);

        /*decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if (visibility == 0){
                    decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
                }
            }
        });*/

        layout = findViewById(R.id.background);

        imageView = findViewById(R.id.imageAiko);
        sceneryView = findViewById(R.id.tokyoNight);
        guyView = findViewById(R.id.imageGuy);
        moodView = findViewById(R.id.imageMood);
        wineView = findViewById(R.id.imageWine);

        button = findViewById(R.id.button);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        button4 = findViewById(R.id.button4);

        textSwitcher = findViewById(R.id.textSwitcher);
        nextButton = findViewById(R.id.imageButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                //If player chose to go with Miki in School2Activity: dialog3
                if(School2Activity.goWithMiki && stringIndex == 44){
                    dialog = dialog3;
                    stringIndex = stringIndex3;
                }
                //If player friendzone Aiko: dialog4
                if(!likeAiko){
                    dialog = dialog4;
                    stringIndex = stringIndex4;
                    likeAiko = true; //To not go to this scope again
                }

                //dialog2: bad guy
                if(isBadGuy){
                    if(stringIndex2 == dialog2.length - 1){
                        isBadGuy = false;
                        stringIndex = 44;
                        textSwitcher.setText(dialog[stringIndex]);
                    } else {
                        textSwitcher.setText(dialog2[++stringIndex2]);
                    } //dialog1: Not bad guy
                } else if (stringIndex == dialog.length-1){ //If last reach index (Dialog1 / Dialog2)
                                                            // - switch to activity depends on players choice before
                    if(School2Activity.goWithMiki){
                        Home3Activity.nightWithMiki = true;
                        playboy.stop();
                        wineSound.stop();
                        Intent intent = new Intent(TokyoActivity.this, BarActivity.class);// New activity
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish(); // Call once you redirect to another activity
                    } else {
                        Home3Activity.nightWithAiko = true;
                        playboy.stop();
                        wineSound.stop();
                        Intent intent = new Intent(TokyoActivity.this, Home3Activity.class);// New activity
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish(); // Call once you redirect to another activity
                    }
                } else {
                    textSwitcher.setText(dialog[++stringIndex]); //Continue String dialog
                }
                screenPopup();
            }
        });
        //Fade in - Fade out for text in the textView
        textSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                textView = new TextView(TokyoActivity.this); //Create new TextView with the settings below
                textView.setTextColor(Color.WHITE);
                textView.setTextSize(15);
                textView.setPadding(50,20,20,20);
                return textView;
            }
        });
        textSwitcher.setText(dialog[stringIndex]); //Start Text
        //Music
        playboy.play(this, R.raw.afternoon);
        playboy.loop(true);

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                button3.setVisibility(View.INVISIBLE);
                button4.setVisibility(View.INVISIBLE);
                textSwitcher.setText("You: Dude you should change your behaviour... You are the worst!");
                stringIndex = 0; //Otherwise screenPopup again
                isBadGuy = true;
            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                button3.setVisibility(View.INVISIBLE);
                button4.setVisibility(View.INVISIBLE);
                textSwitcher.setText("You: I don't think so... I think you are not that bad...");
            }
        });
        //Friendzone
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                button.setVisibility(View.INVISIBLE);
                button2.setVisibility(View.INVISIBLE);
                textSwitcher.setText("You: You are really a good friend for me too, Aiko!");
                likeAiko = false;
            }
        });
        //Like
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                button.setVisibility(View.INVISIBLE);
                button2.setVisibility(View.INVISIBLE);
                textSwitcher.setText("You: I feel the same... I also like you a lot!");
                imageView.setImageResource(R.drawable.aiko_formal_smile_blush);
                moodView.setVisibility(View.VISIBLE);
            }
        });
    }

    public void screenPopup(){

        switch(stringIndex){
            case 12:
                sceneryView.animate().alpha(1).setDuration(1000);
                break;
            case 16:
            case 76:
                guyView.animate().translationX(0).setDuration(1000);
                break;
            case 38:
                button3.setVisibility(View.VISIBLE);
                button4.setVisibility(View.VISIBLE);
                nextButton.setVisibility(View.INVISIBLE);
                break;
            case 44:
            case 73:
                guyView.animate().translationX(-1300).setDuration(1000);
                break;
            case 48:
                imageView.animate().translationX(0).setDuration(1000);
                break;
            case 52:
                imageView.setImageResource(R.drawable.aiko_formal_shout);
                break;
            case 54:
                imageView.setImageResource(R.drawable.aiko_formal_smile_blush);
                break;
            case 61:
                moodView.setVisibility(View.VISIBLE);
                break;
            case 62:
                playboy.stop();
                playboy.play(TokyoActivity.this, R.raw.jazz);
                playboy.loop(true);
                moodView.setVisibility(View.INVISIBLE);
                imageView.setImageResource(R.drawable.aiko_formal_smile);
                layout.setBackgroundResource(R.drawable.ritz_roppongi);
                sceneryView.animate().alpha(0).setDuration(1000);
                break;
            case 70:
                guyView.setImageResource(R.drawable.waiter);
                guyView.animate().translationX(0).setDuration(1000);
                break;
            case 77:
                wineView.animate().alpha(1).setDuration(1000);
                break;
            case 78:
                wineSound.play(TokyoActivity.this, R.raw.glass);
                guyView.animate().translationX(-1300).setDuration(1000);
                break;
            case 79:
                wineView.animate().alpha(0).setDuration(1000);
                break;
            case 82:
                button.setVisibility(View.VISIBLE);
                button2.setVisibility(View.VISIBLE);
                nextButton.setVisibility(View.INVISIBLE);
                break;
        }

    }
    /*@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    // Shows the system bars by removing all the flags
// except for the ones that make the content appear under the system bars.
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void showSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }*/
}
