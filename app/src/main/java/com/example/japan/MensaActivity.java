package com.example.japan;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.example.japan.Helper.BaseActivity;
import com.example.japan.Helper.MusicPlayer;

public class MensaActivity extends BaseActivity {

    private MusicPlayer bellSound = new MusicPlayer();

    private TextSwitcher textSwitcher; //Like TextView but with Fade in - Fade out for TextView
    private ImageButton nextButton;

    private TextView textView;
    private ImageView imageView;
    private ImageView teacherView;
    private ImageView foodView;
    private ImageView mensaView;
    private ImageView moodView;
    private ImageView answerView;

    public static boolean spendTimeMiki = true;

    private int stringIndex = 0;
    private String[] dialog = {
            "The second day in school",
            "You: (Phew that's not so easy to understand everything...)",
            "Watanabe-Sensei: Okay, as we know Tokyo is the capital of Japan!",
            "Watanabe-Sensei: Which city was the capital before? " + LoginActivity.username + " do you know the answer?",
            "Watanabe-Sensei: Alright let's call it a day, don't forget the homeworks!!!",
            "You: (That was a tough lesson!! Okay time for a lunch break!!)",
            "School's Cafeteria", //Case 6
            "You: I'm hungry...",
            "Miki: Hey " + LoginActivity.username + ", how are you today?",
            "You: Oh hey Miki I am doing fine, how about you?",
            "Miki: Me too! Thanks! Hey I have made lots of Onigiri today! Do you wanna have some?",
            "You: Oh really can I?",
            "Miki: Of course!!!",
            "Miki: Here you are!", //Case 13
            "You: Ohhhh looks good!! Arigato, Miki-chan!!",
            "Miki: ahahaha you are welcome!",
            "You: WOAH おいしいいい！ (Yummyyyyyy)",
            "Miki: Oh nice Japanese!",
            "You: Ahahaha just basic phrases, nothing special \uD83D\uDE02",
            "Miki: Hey " + LoginActivity.username + ", I'll wanna show you Asakusa? What do you think shall we go there now?", //Case 19
            //If spend time with Miki:
            "Miki: yay ♥",
            "You: I am so excited!",
            "Miki: Me too ♥ I'm so happy to show you Asakusa!!",
            "You: Can't wait to see Asakusa in real with my own eyes \uD83E\uDD29",
            "Miki: You seem so excited! Asakusa is just around the corner!! じゃ、行きましょう (Alright, let's go!)"
    };
    //If Player rejects Miki invitation but has accepted the first on in School Activity
    private int stringIndex2 = -1;
    private String[] dialog2 = {
            "Miki: Ah.. what a pity... but it can't be helped",
            "You: Sorry Miki \uD83D\uDE14",
            "Miki: I hope we can hang out next time! I wanna guide you in Japan!!",
            "You: I hope so too!",
            "Miki: Okay! Take care " + LoginActivity.username
    };
    //If Player rejects Miki already first time (SchoolActivity.spendTimeMiki = false)
    private int stringIndex3 = -1;
    private String[] dialog3 = {
            "Miki: Ah that's too bad you don't have time again...",
            "You: Yeah I'm so busy when I came to Japan...",
            "Miki: I understand, is it you don't wanna spend time with me? You didn't have time yesterday as well..",
            "You: So sorry Miki! I have to fix my schedule first \uD83D\uDE16 I hope next time!! For sure!",
            "Miki: Okay! Take care " + LoginActivity.username
    };

    private Button button1;
    private Button button2;
    private Button button3;
    private Button button4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mensa);

        button1 = findViewById(R.id.button);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        button4 = findViewById(R.id.button4);

        imageView = findViewById(R.id.imageMiki);
        teacherView = findViewById(R.id.imageTeacher);
        foodView = findViewById(R.id.imageOnigiri);
        mensaView = findViewById(R.id.mensa);
        moodView = findViewById(R.id.imageMood);
        answerView = findViewById(R.id.imageAnswer);

        textSwitcher = findViewById(R.id.textSwitcher);

        nextButton = findViewById(R.id.imageButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                //Dialog 1
                if(spendTimeMiki){
                    if (stringIndex == dialog.length-1){
                        playboy.stop();
                        Intent intent = new Intent(MensaActivity.this, AsakusaActivity.class);// New activity
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish(); // Call once you redirect to another activity
                    } else {
                        textSwitcher.setText(dialog[++stringIndex]); //Else next Text from String
                        screenPopup();
                    } //Dialog 2
                } else if(SchoolActivity.spendTimeMiki){
                    if (stringIndex2 == dialog2.length-1){
                        playboy.stop();
                        Intent intent = new Intent(MensaActivity.this, AkihabaraActivity.class);// New activity
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish(); // Call once you redirect to another activity
                    } else {
                        textSwitcher.setText(dialog2[++stringIndex2]);
                    } //Dialog3
                } else if(stringIndex3 == dialog3.length-1){
                    playboy.stop();
                    Intent intent = new Intent(MensaActivity.this, AkihabaraActivity.class);// New activity
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish(); // Call once you redirect to another activity
                } else {
                    textSwitcher.setText(dialog3[++stringIndex3]);
                }

            }
        });
        //Fade in - Fade out for text in the textView
        textSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                textView = new TextView(MensaActivity.this); //Create new TextView with the settings below
                textView.setTextColor(Color.WHITE);
                textView.setTextSize(15);
                textView.setPadding(50,20,20,20);
                return textView;
            }
        });
        textSwitcher.setText(dialog[stringIndex]); //Start Text
        //Music
        playboy.play(this, R.raw.school);
        playboy.loop(true);

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                button3.setVisibility(View.INVISIBLE);
                button4.setVisibility(View.INVISIBLE);
                textSwitcher.setText("Watanabe-Sensei: That's not correct... It's Kyoto! You should have done your homework...");
                answerView.setImageResource(R.drawable.wrong);
                answerView.setVisibility(View.VISIBLE);
            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                button3.setVisibility(View.INVISIBLE);
                button4.setVisibility(View.INVISIBLE);
                textSwitcher.setText("Watanabe-Sensei: Exactly! Kyoto was the capital from 1180 until 1868, now it's Tokyo!");
                answerView.setVisibility(View.VISIBLE);
                School3Activity.counter++;
            }
        });
        //Go with Miki to Asakusa
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                button1.setVisibility(View.INVISIBLE);
                button2.setVisibility(View.INVISIBLE);
                imageView.setImageResource(R.drawable.miki_pose1_summer_smile_blush);
                textSwitcher.setText("You: That's a great idea!! Yesss why not, let's go there!!");
                moodView.setVisibility(View.VISIBLE);
            }
        });
        //Go with Aiko to Akihabara
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                button1.setVisibility(View.INVISIBLE);
                button2.setVisibility(View.INVISIBLE);
                imageView.setImageResource(R.drawable.miki_pose1_summer_sad);
                textSwitcher.setText("You: Oh that's sounds greaaat but... unfortunately I have to do something..");
                moodView.setImageResource(R.drawable.tear);
                moodView.setVisibility(View.VISIBLE);
                spendTimeMiki = false;
            }
        });
    }

    public void screenPopup(){
        switch(stringIndex){
            case 2:
                teacherView.animate().translationX(0).setDuration(1000);
                break;
            case 3:
                nextButton.setVisibility(View.INVISIBLE);
                button3.setVisibility(View.VISIBLE);
                button4.setVisibility(View.VISIBLE);
                break;
            case 4:
                bellSound.play(MensaActivity.this, R.raw.bell);
                break;
            case 5:
                teacherView.animate().translationXBy(-1300).setDuration(1000);
                answerView.setVisibility(View.INVISIBLE);
                break;
            case 6:
                mensaView.animate().alpha(1).setDuration(1000);
                break;
            case 8:
                bellSound.stop();
                imageView.animate().translationX(0).setDuration(1000);
                break;
            case 13:
                foodView.setVisibility(View.VISIBLE);
                foodView.animate().scaleX(1).scaleY(1).setDuration(1000);
                break;
            case 15:
                foodView.setVisibility(View.INVISIBLE);
                imageView.setImageResource(R.drawable.miki_pose1_summer_smile_blush);
                break;
            case 17:
                imageView.setImageResource(R.drawable.miki_pose1_summer_smile);
                break;
            case 19:
                button1.setVisibility(View.VISIBLE);
                button2.setVisibility(View.VISIBLE);
                nextButton.setVisibility(View.INVISIBLE);
                break;

        }

    }
}
