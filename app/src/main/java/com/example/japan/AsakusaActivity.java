package com.example.japan;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.example.japan.Helper.BaseActivity;

public class AsakusaActivity extends BaseActivity {

    private TextSwitcher textSwitcher; //Like TextView but with Fade in - Fade out for TextView
    private ImageButton nextButton;

    private boolean tellTruth = false;
    private boolean orderOmurice = false;

    private int stringIndex = 0;
    private String[] dialog = {
            "Asakusa",
            "Miki: Welcome to Asakusa!",
            "You: I just have one word for this: BEAUTIFUL!",
            "Miki: I wanna tell you a bit about Asakusa!", //Case 3
            "Miki: The temple here is called Senso-Ji, it is the oldest Buddhist temple in Tokyo!",
            "Miki: There are also many Matsuri (Festivals) held in Asakusa\nYou can eat a lot of delicious food and enjoy the atmosphere",
            "Miki: You know about the Edo period?",
            "You: Edo period?",
            "Miki: Yeah from like 1600 until late 19th century! It was basically the time of the Samurai",
            "You: Oh I know Samurai!!",
            "Miki: Of course there aren't anymore but back then, there were a lot of theaters and also.. how could I say...",
            "Miki: ...Like Adult Activities House, you know like a thriving pleasure corner",
            "You: (A pleasure quarter huh...)",
            "Miki: Haha what's wrong? You kinda smiled when I mentioned pleasure corner", //Case 13
            "You: Ahhh.. I-it's nothing! I just learnt something new! Thanks, Miki",
            "Miki: Okay let's go inside the temple!",
            "You: Sure!!",
            "Miki: Oh you should try Omikuji!",
            "You: Omikuji?",
            "Miki: Yeah it tells your fortune! Let's see if you have luck!",
            "Miki: It costs 100 yen then you have to shake a box to get a number, so you can get your fortune paper!",
            "You: Wow sounds great! Lemme try my fortune!",
            "You: Oh so this is the box, okay shake it",
            "You: Alright, so let's see hmmmmmm",
            "You: So what does it mean?", //Case 24
            "Miki: Ohhhh you have big fortune!! Congrats!",
            "Miki: It also tells you will eventually find someone precious",
            "You: Someone precious? Oh that is unexpected! I hope it will come true",
            "Miki: Hehe are you hungry?", //Case 28
            "You: Yeah I do! Let's go eat?",
            "Miki: Sure! I know a good restaurant nearby! Let's go!",
            "Miki: Okay here we are!!", //Case 31
            "You: This looks fancy here! Thanks!",
            "Miki: The Sushi and the Omurice here is good!",
            "You: What is Omurice?",
            "Miki: It's like Omelet filled with rice! It's really good!",
            "You: Ah okay.. hmmm",
            "Chef: Hello and welcome! What can I cook for you today?", //Case 37
            "You: Oh you are the chef from the other restaurant?",
            "Chef: Ah I remember you! Yeah I am the chef of many restaurants here in Tokyo",
            "You: Wow cool!",
            "Soma: Call me Soma! So have you decided already?", //Case 41
            "Soma: Alright understood!",
            "You: Wow! I saw him already when I was first time went to a restaurant in Tokyo!", //Case 42
            "Miki: Oh is it so? Cool! Btw where do you stay?",
            "You: I'm staying at a friend's place! It's really convenient!",
            "Miki: Nice! So you can save money! It's a male friend, no?", //Case 46
            "Miki: Oh okay! Are you good friends?",
            "You: Uhm yeah kind of, ahhhhh have you ever been abroad?",
            "Miki: huh? Uhm yeah I have been to Germany once! It was so nice!",
            "You: Wow! Awesome!",
            "Soma: Okay food is coming!",
            "You: Woooooow!", //Case 52
            "You: Looks so good!",
            "Miki: Yeaaaah right? Let's eat! Itadakimasu",
            "You: Itadakimasu",
            "You: YUMMY!! Why is Japanese food soooo gooood?!", //case 56
            "Miki: You look happy \uD83D\uDE01",
            "You: Good food makes me HAPPY! Right? \uD83D\uDE0D",
            "Miki: hehe yes!", //case 59
            "You: And what is your favorite food?",
            "Miki: I like typical Japanese Izakaya food!",
            "You: Izakaya is a Japanese pub, right?",
            "Miki: Yup! It's a good place to drink beer and eat delicious food!",
            "You: Wow I wanna go!",
            "Miki: Of course! Next time maybe?",
            "You: Yeah why not \uD83D\uDE1A",
            "Miki: Wow you are a fast eater! Already done haha",
            "You: I could even eat more ahahaha",
            "Miki: You are so funny, " + LoginActivity.username, //Case 69
            "You: Oh am I? \uD83D\uDE07",
            "Miki: It was a great day...",
            "You: Yeah! Let's do it again!! I wanna learn more about Japan, thanks for guiding me!",
            "Miki: You are very welcome! Okay, let's go back soon",
            "You: Yup!!"

    };
    private int stringIndex2 = -1;
    private String[] dialog2 = {
            "Miki: I see... so she is like your girlfriend?",
            "You: No no!!! Just a friend, really!",
            "Miki: But isn't it weird to stay at a girl's place..?",
            "You: Yeah maybe.. but I have my own room! Beside that I don't know any Japanese guy..",
            "Miki: Ahaha I'm just kidding. It's okay!"
    };

    private TextView textView;
    private ImageView mikiView;
    private ImageView omikujiView;
    private ImageView restaurantView;
    private ImageView chefView;
    private ImageView moodView;
    private ImageView omuriceView;
    private ImageView sushiView;

    private Button button;
    private Button button2;
    private Button button3;
    private Button button4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asakusa);

        mikiView = findViewById(R.id.imageMiki);
        moodView = findViewById(R.id.imageMood);
        omikujiView = findViewById(R.id.imageOmikuji);
        restaurantView = findViewById(R.id.restaurantImage);
        chefView = findViewById(R.id.imageChef);
        omuriceView = findViewById(R.id.imageOmurice);
        sushiView = findViewById(R.id.imageSushi);

        button = findViewById(R.id.button);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        button4 = findViewById(R.id.button4);

        textSwitcher = findViewById(R.id.textSwitcher);
        nextButton = findViewById(R.id.imageButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //If not telling the truth about sleeping with a girl:
                if(!tellTruth){
                    if (stringIndex == dialog.length - 1) {
                        playboy.stop();
                        Intent intent = new Intent(AsakusaActivity.this, Home2Activity.class);// New activity
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish(); // Call once you redirect to another activity
                    } else {
                        textSwitcher.setText(dialog[++stringIndex]); //Else next Text from String
                        screenPopup();
                    }
                } else if (stringIndex2 == dialog2.length - 1) {
                    tellTruth = false;
                    moodView.setVisibility(View.INVISIBLE);
                    mikiView.setImageResource(R.drawable.miki_pose1_summer_smile);
                    textSwitcher.setText("Miki: Nevermind, let's have fun ♥");
                    stringIndex = 50; //continue dialog 1
                } else {
                    textSwitcher.setText(dialog2[++stringIndex2]);
                }

            }
        });
        //Fade in - Fade out for text in the textView
        textSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                textView = new TextView(AsakusaActivity.this); //Create new TextView with the settings below
                textView.setTextColor(Color.WHITE);
                textView.setTextSize(15);
                textView.setPadding(50, 20, 20, 20);
                return textView;
            }
        });
        textSwitcher.setText(dialog[stringIndex]); //Start Text
        //Music
        playboy.play(this, R.raw.kimochi);
        playboy.loop(true);
        //Omurice:
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                button3.setVisibility(View.INVISIBLE);
                button4.setVisibility(View.INVISIBLE);
                textSwitcher.setText("You: I want to try the Omurice!");
                orderOmurice = true;
            }
        });
        //Sushi:
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                button3.setVisibility(View.INVISIBLE);
                button4.setVisibility(View.INVISIBLE);
                textSwitcher.setText("You: I would like to have Sushi please!");
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                button.setVisibility(View.INVISIBLE);
                button2.setVisibility(View.INVISIBLE);
                textSwitcher.setText("You: Yeah of course a boy!! (I feel bad for lying..)");

            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                button.setVisibility(View.INVISIBLE);
                button2.setVisibility(View.INVISIBLE);
                textSwitcher.setText("You: Well it's a girl actually..");
                mikiView.setImageResource(R.drawable.miki_pose1_summer_sad);
                moodView.setVisibility(View.VISIBLE);
                tellTruth = true;
            }
        });
    }

    public void screenPopup(){

        switch (stringIndex){
            case 1:
                mikiView.animate().alpha(1).setDuration(1000);
                break;
            case 3:
                mikiView.setImageResource(R.drawable.miki_pose3_summer_smile);
                break;
            case 13:
                mikiView.setImageResource(R.drawable.miki_pose4_summer_shout);
                break;
            case 15:
                mikiView.setImageResource(R.drawable.miki_pose1_summer_smile);
                break;
            case 24:
                omikujiView.animate().alpha(1).setDuration(1000);
                break;
            case 28:
                omikujiView.animate().alpha(0).setDuration(1000);
                break;
            case 31:
                restaurantView.animate().alpha(1).setDuration(1000);
                break;
            case 37:
            case 51:
                chefView.animate().translationX(0).setDuration(1000);
                break;
            case 41:
                button3.setVisibility(View.VISIBLE);
                button4.setVisibility(View.VISIBLE);
                nextButton.setVisibility(View.INVISIBLE);
                break;
            case 42:
                chefView.animate().translationX(-1300).setDuration(1000);
                break;
            case 46:
                button.setVisibility(View.VISIBLE);
                button2.setVisibility(View.VISIBLE);
                nextButton.setVisibility(View.INVISIBLE);
                break;
            case 52:
                if(orderOmurice){
                    omuriceView.animate().alpha(1).setDuration(1000);
                } else {
                    sushiView.setVisibility(View.VISIBLE);
                    sushiView.animate().scaleY(1).scaleX(1).setDuration(1000);
                }
                chefView.animate().translationX(-1300).setDuration(1000);
                break;
            case 56:
                omuriceView.animate().alpha(0).setDuration(1000);
                sushiView.setVisibility(View.INVISIBLE);
                break;
            case 59:
                mikiView.setImageResource(R.drawable.miki_pose2_summer_shout);
                break;
            case 69:
                mikiView.setImageResource(R.drawable.miki_pose1_summer_shout);
                break;
        }
    }
}
