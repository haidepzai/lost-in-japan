package com.example.japan;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.example.japan.Helper.BaseActivity;
import com.example.japan.Helper.MusicPlayer;
import com.example.japan.Helper.TypeWriter;

public class MikiActivity extends BaseActivity implements View.OnClickListener {

    MusicPlayer fireworkPlayer = new MusicPlayer();

    final Handler handler = new Handler(); //Handler for delayed execute

    private TextSwitcher textSwitcher; //Like TextView but with Fade in - Fade out for TextView
    private ImageButton nextButton;
    private TextView textView;

    private TypeWriter dialogTxt;
    private ImageButton nextActivity;


    private ImageView mikiView;
    private ImageView chefView;
    private ImageView matsuriView;
    private ImageView hanabiView;
    private ImageView moodView;
    private ImageView karaageView;

    private int stringIndex = 0;
    private String[] dialog = {
            "At the festival",
            "You: (Wow all these people wearing a Yukata, how beautiful!)",
            "You: (But it is really crowded here.. so difficult to walk)",
            "You: (I was supposed to meet Miki here, where is she?)",
            "You: (Hmmm I hope she is not sick or something... still kinda worried)",
            "You: (Yesterday she didn't feel well...)",
            "You: (I'm worried.. maybe should go to her place)",
            "...",
            "Miki: Hey hey!",
            "You: ?!",
            "Miki: Hi " + LoginActivity.username, //Case 10
            "You: MIKI!",
            "Miki: お待たせしました (I'm sorry to have kept you waiting)",
            "You: No it's okay! I'm glad to see you and you look wonderful in this Yukata!",
            "Miki: T-thanks ♥", //Case 14
            "You: So how are you feeling today?",
            "Miki: Much better! Thanks for asking ♥",
            "You: よかった (Here: I'm glad)", //Case 17
            "You: But I didn't know you about your health condition and I was worried!!",
            "Miki: I didn't want to worry you, so I didn't say...", //Case 19
            "You: Miki...",
            "Miki: Don't worry! Let's walk around!", //Case 21
            "You: Sure!",
            "*Both are enjoying the nice festival atmosphere, talking and laughing a lot together*", //Case 23
            "You: Look at all these food stalls! Everything looks so delicious!",
            "Miki: Let's eat something!",
            "You: Sure! Maybe Takoyaki?",
            "Miki: Yeah Takoyaki is a popular festival food!",
            "You: Oh wait there is the restaurant guy! Let's see what he got!",
            "Miki: Haha sure!",
            "You: Hey yo!",
            "Soma: Oh heyyyyy", //Case 31
            "You: I didn't expect to see you here! Haha \uD83D\uDE02",
            "Soma: Haha I'm everywhere you know!",
            "You: I guess so \uD83D\uDE02",
            "You: Wow this look delicious! What is this?",
            "Soma: Karaage! (Fried chicken) But special one made by me! Wanna have some?",
            "You: Oh sure! からあげ一つをお願いします。(One Karaage please)",
            "Soma: Here you go!", //Case 38
            "You: Ohhhhhhhh~ \uD83D\uDE0D",
            "You: Thank you!", //Case 40
            "Soma: Have fun both of you \uD83D\uDE0F",
            "You: ahahaha thanks! Keep up the good work!",
            "Miki: 美味しそう (Looks delicious)", //Case 43
            "You: いただきます (Itadakimasu)",
            "Miki: mhhhhhhhhhhhhhhhhhh super delicious",
            "You: YESSSSSSSSSSSSS \uD83D\uDE0D",
            "You: I can have more...",
            "Miki: Hehe... so you will leave Japan soon, right? I'm a bit sad but I'm glad we could spend a nice time",
            "You: Yeah... I feel the same... it was too short but I promise I will come back again!",
            "Miki: Let's meet then and go to different places!",
            "You: Of course! Thanks to you I had an awesome time! It was the best time in my life! I will never forget this",
            "Miki: I'm really really happy to hear so ♥", //Case 52
            "You: You are really sweet, Miki...",
            "Miki: " + LoginActivity.username + "... Thank you... sorry I didn't tell you about my condition earlier...", //Case 54
            "Miki: I have some problems with my heart since I was little, so I'm on medication but please don't worry!",
            "You: Miki... Sorry to hear that... Is there not a way to heal that?",
            "Miki: Unfortunately no but it is not severe as it sounds like! I just need to take some rest \uD83D\uDE00",
            "You: You are strong! You can do it!!!!",
            "Miki: I will never give up!!", //Case 59
            "Miki: Oh! The firework will soon begin!!!",
            "You: Ah yeah! Let's watch it together ♥",
            "Miki: Do you know the Japanese word for firework?",
            "You: Hanabi, right?",
            "Miki: Exactly! 花火 (Hanabi)",
            "Miki: Here it goes!!",
            "花火 (Hanabi) Firework", //Case 66
            "You: Woaaaaaah... I have no words for this....",
            "Miki: Beautiful, isn't it?",
            "You: Yeah...",
            "Miki: I'm happy I can spend this moment with you ♥",
            "You: Miki, I like you a lot!",
            "Miki: \uD83D\uDE33", //Case 72
            "You: 好きです！ (I like you)",
            "Miki: " + LoginActivity.username + "...! I like you too!", //Case 74
            "Miki: 私も " + LoginActivity.username + " くんが好きです (I like you too)", //Case 75
            "You: Thank you for everything.. I will never forget this moment and the time in Japan with you, Miki",
            "Miki: Me too, " + LoginActivity.username + "... Let's enjoy this beautiful view ♥",
            "*Both spend the whole night watching the firework*"

    };

    private String dialog2 = "My days in Japan are coming to an end... It was a thrilling week and everything has started " +
            "with Aiko picking me up at the airport... I had a nice time staying at her house but then I met Miki and started " +
            "to like her... I have not regretted my decision. Because of her I could made some wonderful memories... Being with her right now " +
            "and watching the firework is really beautiful end of my trip.. I will come back again...";

    private Button button1;
    private Button button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miki);

        mikiView = findViewById(R.id.yukataMiki);
        chefView = findViewById(R.id.imageChef);
        matsuriView = findViewById(R.id.matsuriImage);
        hanabiView = findViewById(R.id.hanabiImage);
        moodView = findViewById(R.id.imageMood);
        karaageView = findViewById(R.id.imageKaraage);

        button1 = findViewById(R.id.button);
        button2 = findViewById(R.id.button2);

        dialogTxt = findViewById(R.id.dialogText);
        nextActivity = findViewById(R.id.nextActivity);

        textSwitcher = findViewById(R.id.textSwitcher);
        nextButton = findViewById(R.id.imageButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (stringIndex == dialog.length - 1) {
                    nextButton.animate().alpha(0).setDuration(1000);
                    textSwitcher.animate().alpha(0).setDuration(1000);
                    mikiView.animate().alpha(0).setDuration(1000);
                    moodView.animate().alpha(0).setDuration(1000);
                    //Execute after 4 Second
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            fireworkPlayer.stop();
                            dialogTxt.setVisibility(View.VISIBLE);
                            dialogTxt.setCharacterDelay(50);
                            dialogTxt.animateText(dialog2);
                            nextActivity.setVisibility(View.VISIBLE);
                        }
                    }, 4000);

                } else {
                    textSwitcher.setText(dialog[++stringIndex]); //Else next Text from String
                }
                screenPopup();
            }
        });
        //Fade in - Fade out for text in the textView
        textSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                textView = new TextView(MikiActivity.this); //Create new TextView with the settings below
                textView.setTextColor(Color.WHITE);
                textView.setTextSize(15);
                textView.setPadding(50, 20, 20, 20);
                return textView;
            }
        });
        textSwitcher.setText(dialog[stringIndex]); //Start Text
        //Music
        playboy.play(this, R.raw.kimochi);
        playboy.loop(true);

        nextActivity.setOnClickListener(this);
    }

    public void screenPopup() {

        switch (stringIndex){
            case 10:
            case 68:
                mikiView.animate().alpha(1).setDuration(1000);
                break;
            case 14:
                mikiView.setImageResource(R.drawable.miki_yukata_blush);
                moodView.setVisibility(View.VISIBLE);
                break;
            case 17:
            case 21:
                mikiView.setImageResource(R.drawable.miki_yukata_smile);
                moodView.setVisibility(View.INVISIBLE);
                break;
            case 19:
                mikiView.setImageResource(R.drawable.miki_yukata_frown);
                moodView.setImageResource(R.drawable.tear);
                moodView.setVisibility(View.VISIBLE);
                break;
            case 23:
                matsuriView.animate().alpha(1).setDuration(1000);
                break;
            case 31:
                chefView.animate().alpha(1).setDuration(1000);
                break;
            case 38:
                karaageView.animate().alpha(1).setDuration(1000);
                break;
            case 40:
                karaageView.animate().alpha(0).setDuration(1000);
                break;
            case 43:
                chefView.animate().alpha(0).setDuration(1000);
                break;
            case 52:
                mikiView.setImageResource(R.drawable.miki_yukata_blush);
                moodView.setImageResource(R.drawable.heart);
                moodView.setVisibility(View.VISIBLE);
                break;
            case 54:
                mikiView.setImageResource(R.drawable.miki_yukata_frown);
                moodView.setVisibility(View.INVISIBLE);
                break;
            case 59:
                mikiView.setImageResource(R.drawable.miki_yukata_smile);
                break;
            case 66:
                playboy.stop();
                playboy.play(MikiActivity.this, R.raw.intro);
                playboy.loop(true);
                fireworkPlayer.play(MikiActivity.this, R.raw.firework);
                fireworkPlayer.loop(true);
                hanabiView.animate().alpha(1).setDuration(1000);
                mikiView.animate().alpha(0).setDuration(1000);
                break;
            case 72:
                mikiView.setImageResource(R.drawable.miki_yukata_open_blush);
                break;
            case 74:
                mikiView.setImageResource(R.drawable.miki_yukata_blush);
                break;
            case 75:
                moodView.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        playboy.stop();
        Intent intent = new Intent(MikiActivity.this, EndActivity.class);// New activity
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish(); // Call once you redirect to another activity
    }
}
