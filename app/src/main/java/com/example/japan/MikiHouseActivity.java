package com.example.japan;

import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.example.japan.Helper.BaseActivity;
import com.example.japan.Helper.TypeWriter;

public class MikiHouseActivity extends BaseActivity implements View.OnClickListener {
    private ConstraintLayout layout; //For changing background image later

    final Handler handler = new Handler(); //Handler for delayed execute

    private TextSwitcher textSwitcher; //Like TextView but with Fade in - Fade out for TextView
    private ImageButton nextButton;

    private TypeWriter dialogTxt;
    private ImageButton nextActivity;

    private TextView textView;
    private ImageView mikiView;
    private ImageView homeView;
    private ImageView moodView;

    private int stringIndex = 0;
    private String[] dialog = {
            "Miki's house",
            "You: (... I have to tell her that I'll go with Aiko...)",
            "You: (I'm still worried on the other hand why she was absent today)",
            "You: (How should I tell her....)",
            "You: (Ahhhhh it's so hard....)",
            "You: Okay...I'll have to do it",
            "*knocks knocks*",
            "Miki: Ah hey " + LoginActivity.username,
            "You: Hey Miki!!! Are you okay? Where have you been today?",
            "Miki: Hey don't worry! I am fine.. I didn't expect to see you but I'm glad to see you!! ♥",
            "Miki: Sorry " + LoginActivity.username + "... I just didn't feel well..", //case 10
            "You: What's wrong? I and our Sensei were worried!",
            "Miki: I have some health issues and my body is not so strong, you know... so I took a rest",
            "You: Ahhh.. I didn't know that...",
            "Miki: But I am fine now! So did you come to say we will go out tomorrow? ♥", //Case 14
            "You: Actually... I can't...",
            "Miki: ah....",
            "You: I'm so sorry Miki!!!!!!! You are a really nice girl and I really like you a lot... but I cannot...",
            "Miki: I understand... " + LoginActivity.username + "... I really started to like you a lot...",
            "You: I know that!!!!",
            "You: ...",
            "You: But.. You know there is a girl and I'm staying at her house... She always take care of me...",
            "Miki: I see...", //Case 22
            "You: I cannot leave her after all she has done for me...",
            "Miki: " + LoginActivity.username + "... You should be happy with her, it's okay",
            "You: Please understand me, Miki... If things were different maybe we could have a chance..." +
                    "I really wanted to go with you there too...",
            "Miki: It's okay... Please take care of her... and take care of you too...",
            "You: ...",
            "Miki: さようなら (Sayonara)",
            "You: Damn...", //Case 29
            "You: I better go home now",
            "Aiko's House", //Case 31
            "You: ただいま (I'm back)",
            "Aiko: Welcome back, " + LoginActivity.username,
            "You: I feel exhausted...",
            "Aiko: Why? Are you ok?",
            "You: Long story....",
            "Aiko: Oh... if you want you can talk with me about everything ♥", //Case 37
            "You: It's okay... Let's go to the festival tomorrow ♥",
            "Aiko: Y-yes!!!!", //Case 39
            "You: I'm looking forward too!",
            "Aiko: Me too! I'm really really happy! Thanks, " + LoginActivity.username,
            "You: Can't wait for tomorrow!",
            "Aiko: Yesssss!",
            "Aiko: You look a bit tired... you should take a bath and go to bed ♥",
            "You: Yeah! Good night, Aiko",
            "Aiko: Good night, " + LoginActivity.username, //Case 46
            "You: (Okay I need a rest)",
            "You: Ugh...", //Case 48
            "You: I'm so exhausted..."
    };

    public String dialog2 = "Damn I still feel sorry for Miki... she looked very happy when she saw me \uD83D\uDE22" +
            " It breaks my heart to see someone suffers like this... I dunno if I did the right decision but I think going with Aiko" +
            " tomorrow is the better option... After all, she let me stay in her place and I really appreciate it a lot..." +
            " Beside, I can feel she is liking me too and I somehow feel the same... What a dilemma..... I hope tomorrow will be" +
            " a better day!! Time to get some sleep...";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miki_house);

        layout = findViewById(R.id.background);

        dialogTxt = findViewById(R.id.dialogText);
        nextActivity = findViewById(R.id.nextActivity);

        mikiView = findViewById(R.id.imageMiki);
        moodView = findViewById(R.id.imageMood);
        homeView = findViewById(R.id.homeView);

        textSwitcher = findViewById(R.id.textSwitcher);
        nextButton = findViewById(R.id.imageButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                if (stringIndex == dialog.length-1){
                    textSwitcher.setVisibility(View.INVISIBLE);
                    nextButton.setVisibility(View.INVISIBLE);
                    handler.postDelayed(new Runnable() { //Execute after 2 secons
                        @Override
                        public void run() {
                            dialogTxt.setVisibility(View.VISIBLE);
                            dialogTxt.setCharacterDelay(50);
                            dialogTxt.animateText(dialog2);
                            nextActivity.setVisibility(View.VISIBLE);
                        }
                    }, 2000);
                } else {
                    textSwitcher.setText(dialog[++stringIndex]); //Else next Text from String
                }
                screenPopup();
            }
        });
        //Fade in - Fade out for text in the textView
        textSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                textView = new TextView(MikiHouseActivity.this); //Create new TextView with the settings below
                textView.setTextColor(Color.WHITE);
                textView.setTextSize(15);
                textView.setPadding(50,20,20,20);
                return textView;
            }
        });
        textSwitcher.setText(dialog[stringIndex]); //Start Text
        //Music
        playboy.play(this, R.raw.clannad);
        playboy.loop(true);

        nextActivity.setOnClickListener(this);
    }

    public void screenPopup(){
        switch (stringIndex){
            case 7:
                homeView.animate().alpha(1).setDuration(1000);
                mikiView.animate().alpha(1).setDuration(1000);
                break;
            case 10:
                mikiView.setImageResource(R.drawable.miki_work_frown);
                break;
            case 14:
                mikiView.setImageResource(R.drawable.miki_work_smile_blush);
                moodView.setVisibility(View.VISIBLE);
                break;
            case 15:
                playboy.stop();
                break;
            case 16:
                playboy.play(MikiHouseActivity.this, R.raw.kokoro_to_kokoro);
                mikiView.setImageResource(R.drawable.miki_pose4_work_open);
                moodView.setVisibility(View.INVISIBLE);
                break;
            case 22:
                moodView.setImageResource(R.drawable.tear);
                moodView.setVisibility(View.VISIBLE);
                break;
            case 29:
                mikiView.animate().alpha(0).setDuration(1000);
                homeView.animate().alpha(0).setDuration(1000);
                moodView.setVisibility(View.INVISIBLE);
                break;
            case 30:
                mikiView.setImageResource(R.drawable.aiko_smile_front);
                homeView.setImageResource(R.drawable.home);
                playboy.stop();
                break;
            case 31:
                playboy.play(MikiHouseActivity.this, R.raw.home);
                homeView.animate().alpha(1).setDuration(1000);
                break;
            case 33:
                mikiView.animate().alpha(1).setDuration(1000);
                break;
            case 37:
                mikiView.setImageResource(R.drawable.aiko_smile_blush);
                break;
            case 39:
                moodView.setImageResource(R.drawable.heart);
                moodView.setVisibility(View.VISIBLE);
                mikiView.setImageResource(R.drawable.aiko_shout_heartblush);
                break;
            case 46:
                moodView.setVisibility(View.INVISIBLE);
                mikiView.animate().translationX(1300).setDuration(1000);
                break;
            case 47:
                layout.setBackgroundResource(R.drawable.room_night);
                break;
            case 48:
                homeView.animate().alpha(0).setDuration(1000);
                break;

        }

    }

    @Override
    public void onClick(View v) {
        playboy.stop();
        Intent intent = new Intent(MikiHouseActivity.this, TrainActivity.class);// New activity
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish(); // Call once you redirect to another activity
    }
}
