package com.example.japan;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.example.japan.Helper.BaseActivity;
import com.example.japan.Helper.MusicPlayer;

public class School2Activity extends BaseActivity {

    private MusicPlayer bellPlayer = new MusicPlayer();

    private TextSwitcher textSwitcher; //Like TextView but with Fade in - Fade out for TextView
    private ImageButton nextButton;

    public static boolean goWithMiki = true;

    private int stringIndex = 0;
    private String[] dialog = {
            "The third day in school",
            "You: (Damn yesterday was really crazy...)",
            "You: (I couldn't sleep at all...)",
            "Watanabe-Sensei: Hey " + LoginActivity.username + " can you answer this question?",
            "You: (Damn it I totally dozed off..)",
            "Watanabe-Sensei: The backside of the 10-Yen coin represents a Buddhism temple.\nWhich one?", //Case 5
            "You: (Phew)",
            "Miki: *whisper* " + LoginActivity.username + ", are you ok??",
            "You: *whisper* Y-yeah it's nothing! I am just a bit sleepy 'cuz I didn't sleep a lot last night..",
            "Miki: *whisper* Let's go to the rooftop to catch some fresh air afterwards!",
            "You: *whisper* Yeah okay!",
            "You: (I hope the lesson ends soon)",
            "Watanabe-Sensei: Alright that's all for now! Have a nice evening!", //Case 12
            "Watanabe-Sensei: Please do your homework!!",
            "You: Finally!",
            "Miki: Alright, let's go!",
            "You: Yeah!",
            "Miki: Here we are", //Case 17
            "You: Thanks Miki! Too many things going on in my head...",
            "Miki: What's wrong?", //Case 19
            "You: Oh well.. yesterday I met a crazy guy who made some trouble",
            "Miki: OMG, are you really ok?",
            "You: Yeah I am fine, nothing happened but still.. it was a weird situation..",
            "Miki: " + LoginActivity.username + "... is there anything I can do for you?",
            "You: Thanks Miki, you are so kind! But I'm fine by myself, I don't wanna involve you in this story",
            "Miki: But it's okay! We are friends, right?",
            "You: Yeah.. but it's okay, please forget it!",
            "Miki: Alright! How about we go to an Izakaya tonight? A typical Japanese pub??", //Case 27
            "Miki: Very well! Then let's meet tonight okay?",
            "You: Sure!! See you later, Miki!",
            "Miki: I'm looking forward too! See you ♥"

    };

    //If Player rejects Miki invitation but has accepted other Invitation before
    private int stringIndex2 = -1;
    private String[] dialog2 = {
            "Miki: I understand.. yeah you don't feel well but I just wanted to cheer you up",
            "You: Sorry Miki \uD83D\uDE14 when I feel better, let's hang out again!!",
            "Miki: Of course \uD83D\uDE42 Please take your time!",
            "You: Thanks for understanding me Miki! See you next time!!",
            "Miki: Take care!! If you need a talk, feel free to talk to me, " + LoginActivity.username
    };
    //If Player rejects all invitations so far
    private int stringIndex3 = -1;
    private String[] dialog3 = {
            "Miki: Again you don't have time? I think you don't even like me...",
            "You: N-no it's not like that! You know because yesterday I had trouble and yeah...",
            "Miki: Yeah of course.. but I can help you!!! Seems you don't want to spend time with me",
            "You: Please don't think so... I'm just sorry...",
            "Miki: It's okay.. I hope you will be fine soon, " + LoginActivity.username
    };

    private TextView textView;
    private ImageView mikiView;
    private ImageView teacherView;
    private ImageView rooftopView;
    private ImageView moodView;
    private ImageView answerView;

    private Button button1;
    private Button button2;
    private Button button3;
    private Button button4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school2);

        mikiView = findViewById(R.id.imageMiki);
        teacherView = findViewById(R.id.imageTeacher);
        rooftopView = findViewById(R.id.rooftop);
        moodView = findViewById(R.id.imageMood);
        answerView = findViewById(R.id.imageAnswer);

        button1 = findViewById(R.id.button);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        button4 = findViewById(R.id.button4);

        textSwitcher = findViewById(R.id.textSwitcher);
        nextButton = findViewById(R.id.imageButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){

                //Dialog 1
                if(goWithMiki){
                    if (stringIndex == dialog.length-1){
                        playboy.stop();
                        bellPlayer.stop();
                        Intent intent = new Intent(School2Activity.this, TokyoActivity.class);// New activity
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish(); // Call once you redirect to another activity
                    } else {
                        textSwitcher.setText(dialog[++stringIndex]); //Else next Text from String
                        screenPopup();
                    } //Dialog 2
                } else if(SchoolActivity.spendTimeMiki || MensaActivity.spendTimeMiki){
                    if (stringIndex2 == dialog2.length-1){
                        playboy.stop();
                        bellPlayer.stop();
                        Intent intent = new Intent(School2Activity.this, TokyoActivity.class);// New activity
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish(); // Call once you redirect to another activity
                    } else {
                        textSwitcher.setText(dialog2[++stringIndex2]);
                    } //Dialog3
                } else if(stringIndex3 == dialog3.length-1){
                    playboy.stop();
                    bellPlayer.stop();
                    Intent intent = new Intent(School2Activity.this, TokyoActivity.class);// New activity
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish(); // Call once you redirect to another activity
                } else {
                    textSwitcher.setText(dialog3[++stringIndex3]);
                }
            }
        });
        //Fade in - Fade out for text in the textView
        textSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                textView = new TextView(School2Activity.this); //Create new TextView with the settings below
                textView.setTextColor(Color.WHITE);
                textView.setTextSize(15);
                textView.setPadding(50,20,20,20);
                return textView;
            }
        });
        textSwitcher.setText(dialog[stringIndex]); //Start Text
        //Music
        playboy.play(this, R.raw.school);
        playboy.loop(true);

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                button3.setVisibility(View.INVISIBLE);
                button4.setVisibility(View.INVISIBLE);
                textSwitcher.setText("Watanabe-Sensei: Exactly! The 10-Yen coin showing the Phoenix Hall (平等院鳳凰堂)");
                answerView.setVisibility(View.VISIBLE);
                School3Activity.counter++;
            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                button3.setVisibility(View.INVISIBLE);
                button4.setVisibility(View.INVISIBLE);
                textSwitcher.setText("Watanabe-Sensei: Wrong!! It's the Byodo-In temple!\n"
                        + LoginActivity.username + " you look quite absent today! Please focus more on the lesson!");
                answerView.setImageResource(R.drawable.wrong);
                answerView.setVisibility(View.VISIBLE);
            }
        });

        //Go with Miki to Izakaya
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                button1.setVisibility(View.INVISIBLE);
                button2.setVisibility(View.INVISIBLE);
                mikiView.setImageResource(R.drawable.miki_pose1_winter_shout_blush);
                textSwitcher.setText("You: Oh I always wanted to go there, yes why not! Sure!!!");
                moodView.setVisibility(View.VISIBLE);
            }
        });
        //Go to Aiko's Home
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                button1.setVisibility(View.INVISIBLE);
                button2.setVisibility(View.INVISIBLE);
                mikiView.setImageResource(R.drawable.miki_pose4_winter_frown);
                textSwitcher.setText("You: Ugh I have to go home today immediately actually...");
                moodView.setImageResource(R.drawable.tear);
                moodView.setVisibility(View.VISIBLE);
                goWithMiki = false;
            }
        });
    }

    public void screenPopup(){
        switch(stringIndex) {
            case 3:
                teacherView.animate().translationX(0).setDuration(1000);
                break;
            case 5:
                nextButton.setVisibility(View.INVISIBLE);
                button3.setVisibility(View.VISIBLE);
                button4.setVisibility(View.VISIBLE);
                break;
            case 7:
                teacherView.animate().translationX(-1300).setDuration(1000);
                answerView.setVisibility(View.INVISIBLE);
                mikiView.animate().translationX(0).setDuration(1000);
                break;
            case 11:
                mikiView.animate().translationX(1300).setDuration(1000);
                break;
            case 12:
                teacherView.animate().translationX(0).setDuration(1000);
                bellPlayer.play(School2Activity.this, R.raw.bell);
                break;
            case 14:
                teacherView.animate().translationX(-1300).setDuration(1000);
                break;
            case 15:
                mikiView.animate().translationX(0).setDuration(1000);
                break;
            case 17:
                rooftopView.animate().alpha(1).setDuration(1000);
                break;
            case 19:
                mikiView.setImageResource(R.drawable.miki_pose4_winter_open);
                break;
            case 27:
                mikiView.setImageResource(R.drawable.miki_pose1_winter_smile);
                nextButton.setVisibility(View.INVISIBLE);
                button1.setVisibility(View.VISIBLE);
                button2.setVisibility(View.VISIBLE);
                break;
        }
    }
}
