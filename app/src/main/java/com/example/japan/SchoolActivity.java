package com.example.japan;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.example.japan.Helper.BaseActivity;
import com.example.japan.Helper.MusicPlayer;

public class SchoolActivity extends BaseActivity {

    private MusicPlayer bellPlayer = new MusicPlayer();

    private TextSwitcher textSwitcher; //Like TextView but with Fade in - Fade out for TextView
    private ImageButton nextButton;

    public static boolean spendTimeMiki = true;

    private int stringIndex = 0;
    private String[] dialog = {
            "First day in school",
            "You: So this is my school huh I feel like living in an anime!! Damn I sound like a weeb \uD83E\uDD13",
            "Student: Hey! Are you the new exchange student?",
            "You: Yeah I am " + LoginActivity.username + ", it's nice to meet you!",
            "Miki: I am Miki, nice to meet you as well! Seems we are in the same class! If you have any questions ask me anytime!",
            "You: Oh thanks! I appreciate it!",
            "Teacher: Hello everyone! I am Sensei Watanabe! Your Japanese and English teacher!!",
            "Watanabe-Sensei: Ohhh we have a new student here! Welcome to our wonderful class!",
            "You: The pleasure is all mine!",
            "Watanabe-Sensei: Everyone please be nice to him and become friend with him!",
            "Miki: *whisper* She's a really nice teacher! Kind and humble!! If there is a problem you can always go to her",
            "You: Oh is it so? Wow \uD83D\uDE32",
            "Watanabe-Sensei: Okay! Let's start with our lesson!!",
            "You: (Damn the weaboo soul inside me is literally burning! And boy.. she looks hot lol)",
            "You: (This is too good to be real haha)",
            "You: (My dream was always to eat a Bento on the school's rooftop hehe)",
            "Miki: Hey why are you smiling all the time? Something wrong?", //case 16
            "You: O-oh nothing I was just thinking how nice it is to be here!",
            "Miki: Haha you better focus on the lesson!",
            "Watanabe-Sensei: Hey guys please be a bit more quiet!",
            "You: Yes ma'am!",
            "You: (Okay man I should focus now!!!!!!!!)",
            "You: (Phew.. it's not so easy though..)",
            "...",
            "Watanabe-Sensei: Alright! That's all for today! Please do your homework!",
            "Miki: Hey " + LoginActivity.username + ", do you wanna hang out a bit?", //case 25
            //Answer 2 (Hang out with Miki):
            "You: Oh sure! Let's hang out!",
            "Miki: Yay ♥ Let's go to the rooftop",
            "School's rooftop", //Case 28
            "Miki: Okay, here we are!",
            "You: Wow!! This is such a nice place!! What an amazing view and the chilling breeze is so refreshing!!",
            "Miki: Yeah it's one of my favorite places",
            "You: Thanks for showing me this place! I always wanted to go to the rooftop and eat お弁当 (Obento - Japanese lunchbox)!!",
            "Miki: Really? Ahhh my mother made two Bentos today! If you want I can give you one!!",
            "You: For real? Thaaaaaank you! I wanna have one!",
            "Miki: Yessss, here you go!",
            "*Both of you spend a nice evening together on the rooftop eating Bento*", //Case 36
            "You: Woah! That smells so good and looks delicious!!! I'm starving!! ITADAKIMASU",
            "Miki: Hahahaha then let's eat!!",
            "You: \uD83E\uDD24 \uD83E\uDD24 \uD83E\uDD24",
            "You: That was good... Thanks a lot, Miki!!",
            "Miki: You are very welcome!!", //Case 41
            "You: Thanks for the food, you are really kind!! I guess this is the Japanese hospitality",
            "Miki: Hahaha it's nothing! I like to make new friends from abroad ♥", //Case 43
            "You: Likewise!!",
            "Miki: So what is your plan here in Japan?",
            "You: Oh that's a good question!! I wanna travel to many places and eat delicious food and also meet many nice people",
            "Miki: That sounds great! I hope you will have a wonderful time here!! Where do you want to go?",
            "You: Thank you Miki \uD83D\uDE00 Mhhhh I definitely wanna visit Asakusa!",
            "Miki: You're welcome, " + LoginActivity.username + "! I see! It's not so far from here, we can go there next time!",
            "You: Yeah why not!!! \uD83E\uDD29 Alright, I will head back home soon.. It was so fun, let us hang out again!",
            "Miki: Anytime! Take care, " + LoginActivity.username,
            "You: See you Miki!"
    };
    //Answer 1 (Hang out with Aiko)
    private int stringIndex2 = 0;
    private String[] dialog2 = {
            "You: Oh thanks for the invitation but uhm.. I have something to do! Maybe next time?",
            "Miki: Ah okay... sure! See you then!"
    };

    private TextView textView;
    private ImageView mikiView;
    private ImageView teacherView;
    private ImageView rooftopView;
    private ImageView moodView;
    private ImageView bentoView;

    private Button button1;
    private Button button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school);

        mikiView = findViewById(R.id.imageMiki);
        teacherView = findViewById(R.id.imageTeacher);
        rooftopView = findViewById(R.id.rooftop);
        moodView = findViewById(R.id.imageMood);
        bentoView = findViewById(R.id.bento);

        button1 = findViewById(R.id.button);
        button2 = findViewById(R.id.button2);

        textSwitcher = findViewById(R.id.textSwitcher);
        nextButton = findViewById(R.id.imageButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){

                if(spendTimeMiki) {
                    if (stringIndex == dialog.length-1){
                        playboy.stop();
                        bellPlayer.stop();
                        Intent intent = new Intent(SchoolActivity.this, RoomActivity.class);// New activity
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish(); // Call once you redirect to another activity
                    } else {
                        textSwitcher.setText(dialog[++stringIndex]); //Else next Text from String
                    }
                } else if (stringIndex2 == dialog2.length-1){
                    playboy.stop();
                    bellPlayer.stop();
                    Intent intent = new Intent(SchoolActivity.this, OkonomiyakiActivity.class);// New activity
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish(); // Call once you redirect to another activity
                } else {
                    textSwitcher.setText(dialog2[++stringIndex2]);
                }
                screenPopup();
            }
        });
        //Fade in - Fade out for text in the textView
        textSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                textView = new TextView(SchoolActivity.this); //Create new TextView with the settings below
                textView.setTextColor(Color.WHITE);
                textView.setTextSize(15);
                textView.setPadding(50,20,20,20);
                return textView;
            }
        });
        textSwitcher.setText(dialog[stringIndex]); //Start Text
        //Music
        playboy.play(this, R.raw.school);
        playboy.loop(true);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                textSwitcher.setText(dialog2[0]);
                button1.setVisibility(View.INVISIBLE);
                button2.setVisibility(View.INVISIBLE);
                mikiView.setImageResource(R.drawable.miki_casual_frown);
                moodView.setImageResource(R.drawable.tear);
                moodView.setVisibility(View.VISIBLE);
                spendTimeMiki = false;
                stringIndex = 0; //Otherwise Buttons will show up again..
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                stringIndex = 26;
                textSwitcher.setText(dialog[stringIndex]);
                button1.setVisibility(View.INVISIBLE);
                button2.setVisibility(View.INVISIBLE);
                RoomActivity.bentoMiki = true;
            }
        });
    }

    public void screenPopup(){
        switch(stringIndex){
            case 2:
                mikiView.animate().alpha(1).setDuration(1000);
                break;
            case 6:
                teacherView.setVisibility(View.VISIBLE);
                teacherView.animate().translationXBy(1300).setDuration(1000);
                break;
            case 13:
            case 20:
                teacherView.animate().translationXBy(-1300).setDuration(1000);
                mikiView.animate().translationX(1500).setDuration(1000);
                break;
            case 16:
                mikiView.animate().translationX(0).setDuration(1000);
                break;
            case 19:
                teacherView.animate().translationXBy(1300).setDuration(1000);
                break;
            case 24:
                bellPlayer.play(SchoolActivity.this, R.raw.bell);
                teacherView.animate().translationXBy(1300).setDuration(1000);
                break;
            case 25:
                mikiView.animate().translationX(0).setDuration(1000);
                teacherView.animate().translationXBy(-1300).setDuration(1000);
                button1.setVisibility(View.VISIBLE);
                button2.setVisibility(View.VISIBLE);
                nextButton.setVisibility(View.INVISIBLE);
                break;
            case 27:
                mikiView.setImageResource(R.drawable.miki_casual_smile_blush);
                moodView.setVisibility(View.VISIBLE);
                break;
            case 28:
                rooftopView.animate().alpha(1).setDuration(1000);
                moodView.setVisibility(View.INVISIBLE);
                break;
            case 31:
                mikiView.setImageResource(R.drawable.miki_casual_smile_pose2);
                break;
            case 36:
                bentoView.animate().alpha(1).setDuration(1000);
                break;
            case 40:
                bentoView.animate().alpha(0).setDuration(1000);
                break;
            case 41:
                mikiView.setImageResource(R.drawable.miki_casual_bigsmile_blush_pose2);
                moodView.setVisibility(View.VISIBLE);
                break;
            case 43:
                mikiView.setImageResource(R.drawable.miki_casual_smile_pose2);
                moodView.setVisibility(View.INVISIBLE);
                break;
        }
    }
}
