package com.example.japan;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.example.japan.Helper.BaseActivity;


public class AkihabaraActivity extends BaseActivity {

    private TextSwitcher textSwitcher; //Like TextView but with Fade in - Fade out for TextView
    private ImageButton nextButton;

    private int stringIndex = 0;
    private String[] dialog = {
            "Akihabara - Anime and Electric City",
            "You: (Damn Akihabara at daytime is a crazy place as well)",
            "You: (So many Otakus.. just like me! Look at all those Anime store! I can stay here forever)",
            "Aiko: Heyyyyyyyyyyyyy",
            "Aiko: Hello " + LoginActivity.username, //Case 4
            "You: Ahhh Aiko, how are you?",
            "Aiko: I am fine and you? How was school?",
            "You: Yeah it was great! I ate Onigiri today with my classmate!",
            "Aiko: Wow! You could made some friends already haha \uD83D\uDE06 is she a girl?",
            "You: Yeah! She is also very kind just like you!",
            "Aiko: Hahaha good to hear!",
            "You: So what's your plan here?",
            "Aiko: Since you like Anime, how about we walk around?",
            "You: Sure let's go!",
            "Aiko: Okay ♥",
            "Aiko: Do you know in Akihabara there are lots of game center, weird cafe like " +
                    "Ninja cafe or Maid cafe, anime store where you can buy DVD or figures!",
            "You: Basically the perfect place for me!",
            "Aiko: I guess so! It must be heaven for someone who loves Anime like you!",
            "You: It is indeed haha \uD83E\uDD2A",
            "Aiko: Right here in the corner there is a little Taiyaki stall, wanna try?",
            "You: Taiyaki? You mean those cake filled with red beans or custard sauce in form of a fish?",
            "Aiko: YUP! Really sweet and delicious ♥",
            "You: Of course! I wanna try!",
            "Aiko: Okay there is it!!!!",
            "You: Ahhhh it smells nice!",
            "*Both arrived at the Taiyaki stall*",
            "Aiko: たい焼き二つお願いします！ (Two Taiyaki please)",
            "You: Oh what did you just say?",
            "Aiko: I just ordered 2 Taiyaki!",
            "You: Ah I see Taiyaki Futatsu Onegaishimasu?",
            "Aiko: Yup! Here you go!", //Case 30
            "You: Wow it looks cute! Okay I try!",
            "You: OH it's hot!!! But soooo good and fluffy wow niiiice",
            "Aiko: You look happy!",
            "You: Yeah I am it's so goooood",
            "Aiko: Alright, how about we go to a Maid cafe next?",
            "You: Ohhh yeah I always wanted to visit one! Let's go!",
            "Aiko: Okay!!!",
            "Maid Cafe Maidreamin", //Case 38
            "You: Haha it's a cute place!",
            "Aiko: Yeah haha kinda... weird but funny \uD83D\uDE02 \uD83D\uDE02",
            "Maid: Welcome back master!", //Case 41
            "You: Oh hello!",
            "Maid: First you have to wear this bunny ears!!",
            "You: Oh what? LOL",
            "Maid: Now you are ready to order! What can I do for you my master?",
            "You: Uh-uhmmm ahh I wanna try this Tonkatsu!",
            "Maid: Understood. What about you Ojou-Sama?",
            "Aiko: ストロベリーパフェお願いします (Strawberry Parfait please)",
            "Maid: 畏まりました (Understood)",
            "You: So you ordered a strawberry parfait?", //Case 50
            "Aiko: Yup! It's my favorite ♥",
            "You: Ahaha you look happy!",
            "Aiko: I am always happy when I can eat delicious food!",
            "You: Me too!",
            "Aiko: So, " + LoginActivity.username + " what kind of girl do you like?",
            "You: Oh why so sudden? Now that's a difficult question!",
            "You: Mhhhh \uD83E\uDD14",
            //Not sure
            "Aiko: Oh I see! Hehe", //Index 58
            "You: Why? How about you?",
            "Aiko: Just out of curiosity! Well as for me I like someone who is honest and makes me smile haha",
            "You: Oh cool!",
            "Aiko: Yeah I hope to find my dream man some day haha",
            "You: Hahaha I hope so too!",
            "Maid: Sorry for the wait! Here is the Strawberry Parfait for you my mistress", //Case 64
            "You: Woaaaaaah looks good!",
            "Aiko: Ohhhhh yesssssss!!",
            "Maid: And for you master, the delicious Tonkatsu!",
            "You: LOOKS GOOD!", //Case 69
            "Maid: But before we eat, we have to enhance it with special maid magic!",
            "You: Maid magic?",
            "Maid: Yesss please perform a heart with your hand and say Moe-Moe-Kyun!!",
            "You: Uhm whaaat?",
            "Maid: Now let's go!",
            "Everyone: Moe-Moe-Kyuuuuuuuuun", //Case 74
            "Maid: Well done! Please enjoy your meal!", //Case 75
            "You: Wow that was funny",
            "Aiko: Ahahahahah you look so embarrassed!",
            "You: Yeah.. that was weird.. anyway let's eaaat!",
            "Both: ITADAKIMASU",
            "Nomomomomom \uD83E\uDD24",
            "You: Super good!",
            "Aiko: I am glad to spend time with you! It's never boring ♥",
            "You: Me too! Thanks Aiko!",
            "Aiko: Let's do it again!!",
            "You: Sure!!",
            "Aiko: Hehe that was so good! The parfait was just perfect!!",
            "You: You looked very happy!",
            "Aiko: Also thanks to you!",
            "You: Can't wait which place you wanna show me next \uD83D\uDE0D",
            "Aiko: Sure! Alright let's go home!"

    };
    private boolean likeAiko = false;
    private int stringIndex2 = -1;
    private String[] dialog2 = {
            "Aiko: Oh really? \uD83D\uDE33",
            "You: Yeah I feel comfortable with you!",
            "Aiko: Wow I feel flattered! Thanks! I like you too",
            "You: Haha I hope we can have a lot of fun!",
            "Aiko: Yeah I hope so too!"
    };

    private TextView textView;
    private ImageView imageView;
    private ImageView taiyakiView;
    private ImageView maidCafe;
    private ImageView maidView;
    private ImageView parfaitView;
    private ImageView tonkatsuView;
    private ImageView moodView;
    private ImageView moeView;

    private Button button1;
    private Button button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_akihabara);

        button1 = findViewById(R.id.button);
        button2 = findViewById(R.id.button2);

        imageView = findViewById(R.id.imageAiko);
        taiyakiView = findViewById(R.id.imageTaiyaki);
        maidCafe = findViewById(R.id.maidCafe);
        maidView = findViewById(R.id.imageMaid);
        tonkatsuView = findViewById(R.id.tonkatsu);
        parfaitView = findViewById(R.id.imageParfait);
        moodView = findViewById(R.id.imageMood);
        moeView = findViewById(R.id.moeKyu);

        textSwitcher = findViewById(R.id.textSwitcher);

        nextButton = findViewById(R.id.imageButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!likeAiko){
                    if (stringIndex == dialog.length - 1) {
                        playboy.stop();
                        Intent intent = new Intent(AkihabaraActivity.this, Home2Activity.class);// New activity
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish(); // Call once you redirect to another activity
                    } else {
                        textSwitcher.setText(dialog[++stringIndex]); //Else next Text from String
                        screenPopup();
                    }
                } else if (stringIndex2 == dialog2.length - 1) {
                    likeAiko = false;        //To go back to dialog1
                } else {
                    textSwitcher.setText(dialog2[++stringIndex2]);
                }
            }
        });
        //Fade in - Fade out for text in the textView
        textSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                textView = new TextView(AkihabaraActivity.this); //Create new TextView with the settings below
                textView.setTextColor(Color.WHITE);
                textView.setTextSize(15);
                textView.setPadding(50, 20, 20, 20);
                return textView;
            }
        });
        textSwitcher.setText(dialog[stringIndex]); //Start Text
        //Music
        playboy.play(this, R.raw.akihabara);
        playboy.loop(true);
        //Girl like you:
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                button1.setVisibility(View.INVISIBLE);
                button2.setVisibility(View.INVISIBLE);
                textSwitcher.setText("You: I like someone nice and kind.. well someone just like you, Aiko!");
                imageView.setImageResource(R.drawable.aiko_summer_shout_blush);
                moodView.setVisibility(View.VISIBLE);
                likeAiko = true;
            }
        });
        //Not sure
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                button1.setVisibility(View.INVISIBLE);
                button2.setVisibility(View.INVISIBLE);
                textSwitcher.setText("You: Actually I am not sure... I really never thought about this!\nShe should be kind and someone I can feel comfortable with");
                stringIndex = 57;
            }
        });
    }

    public void screenPopup() {
        switch (stringIndex) {
            case 4:
            case 40:
                imageView.animate().alpha(1).setDuration(1000);
                break;
            case 30:
                taiyakiView.setVisibility(View.VISIBLE);
                taiyakiView.animate().scaleX(1).scaleY(1).setDuration(1000);
                break;
            case 32:
                imageView.setImageResource(R.drawable.aiko_summer_smile_blush);
                taiyakiView.setVisibility(View.INVISIBLE);
                break;
            case 33:
                moodView.setVisibility(View.VISIBLE);
                break;
            case 35:
            case 16:
            case 55:
            case 84:
                imageView.setImageResource(R.drawable.aiko_summer_smile);
                moodView.setVisibility(View.INVISIBLE);
                break;
            case 38:
                imageView.animate().alpha(0).setDuration(1000);
                maidCafe.animate().alpha(1).setDuration(1000);
                break;
            case 41:
                maidView.animate().translationX(0).setDuration(1000);
                break;
            case 50:
            case 76:
                maidView.animate().translationX(-1200).setDuration(1000);
                break;
            case 14:
            case 51:
            case 82:
                imageView.setImageResource(R.drawable.aiko_summer_smile_blush);
                moodView.setVisibility(View.VISIBLE);
                break;
            case 57:
                nextButton.setVisibility(View.INVISIBLE);
                button1.setVisibility(View.VISIBLE);
                button2.setVisibility(View.VISIBLE);
                break;
            case 64:
                imageView.setImageResource(R.drawable.aiko_summer_smile);
                moodView.setVisibility(View.INVISIBLE);
                maidView.animate().translationX(0).setDuration(1000);
                break;
            case 65:
                parfaitView.setVisibility(View.VISIBLE);
                parfaitView.animate().scaleY(1).scaleX(1).setDuration(1000);
                break;
            case 67:
                parfaitView.setVisibility(View.INVISIBLE);
                break;
            case 68:
                tonkatsuView.animate().alpha(1).setDuration(1000);
                break;
            case 74:
                moeView.setVisibility(View.VISIBLE);
                moeView.animate().scaleX(2).scaleY(2).setDuration(2000);
                break;
            case 75:
                moeView.setVisibility(View.INVISIBLE);
                tonkatsuView.setVisibility(View.INVISIBLE);
                break;
        }
    }
}
