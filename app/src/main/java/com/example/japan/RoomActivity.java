package com.example.japan;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.example.japan.Helper.BaseActivity;
import com.example.japan.Helper.MusicPlayer;
import com.example.japan.Helper.TypeWriter;

public class RoomActivity extends BaseActivity implements View.OnClickListener {

    MusicPlayer playerSleep = new MusicPlayer();

    final Handler handler = new Handler(); //Handler for delayed execute

    private TypeWriter dialogTxt;
    private ImageButton nextBtn;
    private ImageView roomView;

    public static boolean akihabaraAiko = false;
    public static boolean okonomiyakiAiko = false;
    public static boolean bentoMiki = false;

    private String dialog = "What an amazing day! My new life here in Japan is so thrilling! " +
            " This is exactly the life I've always dreamed of!! " +
            "Okonomiyaki was so great and Akihabara was the bomb! It was a good decision to do an " +
            "exchange week here in Japan! Never regretted my decision! I hope I can see a lot of more things " +
            "with Aiko... I dunno but she is kinda sweet and I somehow like her..." +
            "I should spend more time with her!! But for now, time to sleep...";

    private String dialog2 = "Didn't know my second day would be that nice! It still feels like a dream " +
            "to be here but I am happy that I made that decision to come here! I could eat delicious " +
            "Okonomiyaki with Aiko!! It was so nice.. now I'm still thinking if I should have gone with her " +
            "to Akihabara.. would be also nice I guess.. but I feel exhausted a bit.. well next time! " +
            "On the other hand maybe I should have spent time with Miki... mhhh too many decisions ahhh..." +
            "Time to get some sleep...";

    private String dialog3 = "Damn Aiko didn't talk to me the whole evening.. Of course I promised her to " +
            "go dinner with her today... but I couldn't resist the invitation from Miki... She is so kind " +
            "but on the other hand both are super sweet to me... the Bento with her on the rooftop was like heaven " +
            "I hope Aiko will understand that I am a bit busy with my new life here in Japan... Anyways time to get some sleep...";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);

        dialogTxt = findViewById(R.id.dialogText);
        nextBtn = findViewById(R.id.nextButton);
        roomView = findViewById(R.id.roomMorning);

        if(okonomiyakiAiko){
            dialog = dialog2;
        } else if (bentoMiki) {
            dialog = dialog3;
        }

        //Execute after 2 Second

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                dialogTxt.setVisibility(View.VISIBLE);
                dialogTxt.setCharacterDelay(50);
                dialogTxt.animateText(dialog);
                nextBtn.setVisibility(View.VISIBLE);
            }
        }, 2000);

        nextBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        dialogTxt.setVisibility(View.INVISIBLE);
        nextBtn.setVisibility(View.INVISIBLE);
        playerSleep.play(this,R.raw.goodnight);
        roomView.animate().alpha(1).setDuration(3000);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                playerSleep.stop();
                Intent intent = new Intent(RoomActivity.this, MensaActivity.class);// New activity
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish(); // Call once you redirect to another activity
            }
        }, 9000);
    }
}
