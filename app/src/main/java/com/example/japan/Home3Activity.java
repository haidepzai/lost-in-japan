package com.example.japan;

import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.example.japan.Helper.BaseActivity;
import com.example.japan.Helper.MusicPlayer;
import com.example.japan.Helper.TypeWriter;

public class Home3Activity extends BaseActivity {

    private ConstraintLayout layout; //For changing background image later

    final Handler handler = new Handler(); //Handler for delayed execute

    MusicPlayer playerSleep = new MusicPlayer();

    private TextSwitcher textSwitcher; //Like TextView but with Fade in - Fade out for TextView
    private ImageButton nextButton;

    private TypeWriter dialogTxt;
    private TextView textView;
    private ImageButton nextActivity;

    private ImageView roomView;
    private ImageView moodView;
    private ImageView imageView;

    public static boolean nightWithMiki = false;
    public static boolean nightWithAiko = false;
    //If spent time with Aiko before:
    private int stringIndex = 0;
    private String[] dialog = {
            "Aiko's House",
            "Aiko: ただいま (Tadaima: A phrase when you come back home)",
            "Aiko: It was fun today!",
            "You: Yeah.. thanks for that!",
            "Aiko: Thank you that you spent time with me ♥", //Case 4
            "You: Thanks to you I could spend a nice time in Japan... This trip is unforgettable!\n" +
                    "These memories with you are very precious for me!",
            "Aiko: I'm glad you have fun so far!",
            "Aiko: Tomorrow will be your last day in school, isn't it? You should go to sleep soon!",
            "You: Yup! Have a good night, Aiko!",
            "Aiko: おやすみ, " + LoginActivity.username
    };
    //If spent time with Miki before:
    private int stringIndex2 = 0;
    private String[] dialog2 = {
            "Aiko's house",
            "Aiko: Hey " + LoginActivity.username + " welcome back home! Where have you been?",
            "You: Hey Aiko! Sorry for being late... I met a classmate!",
            "Aiko: I see... did you have fun?",
            "You: Yeah! How about you? How was your day?",
            "Aiko: Well... I waited for you since I was worried because of yesterday!",
            "You: Oh you mean because of Takuya? It's okay... Actually I met him before..",
            "Aiko: You met him before? What happened?", //case 7.2
            "You: Don't worry.. everything is okay!! Just a smalltalk",
            "Aiko: If you say so... anyways good night!",
            "You: Good night, Aiko!"
    };
    //If spent time with Aiko before:
    private String dialog3 = "What an amazing night! Too bad my time in Japan is almost coming to an end... " +
            "Tomorrow is my last school day and then there will be the festival... " +
            "I really should think carefully with whom I will go there... This is not an easy decision \uD83D\uDE2D " +
            "I really had a good time here... Even though I couldn't see everything but it was one of the best time in my life... " +
            "Alright.. time to sleep...";
    //If spent time with Miki before:
    private String dialog4 = "Ugh.. maybe I should have gone home after school to see after Aiko... " +
            "She seems upset and worried about me... But I just couldn't deny the offer from Miki... " +
            "Tomorrow will be my last day in school and then there is an upcoming festival... " +
            "I really have to think carefully with whom I will go there... What a hard decision \uD83D\uDE2D " +
            "Time flies and I really had a good time here... Wish I could stay here longer and see more of Japan! " +
            "This is definitely the best time in my life... Okay... try to sleep a bit...";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home3);

        dialogTxt = findViewById(R.id.dialogText);
        nextActivity = findViewById(R.id.nextButton);

        if(nightWithMiki){
            dialog = dialog2;
            stringIndex = stringIndex2;
            dialog3 = dialog4;
        }
        layout = findViewById(R.id.background);
        roomView = findViewById(R.id.roomNight);
        imageView = findViewById(R.id.imageAiko);
        moodView = findViewById(R.id.imageMood);
        textSwitcher = findViewById(R.id.textSwitcher);
        nextButton = findViewById(R.id.imageButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                if (stringIndex == dialog.length-1){
                    roomView.animate().alpha(1).setDuration(1000);
                    textSwitcher.setVisibility(View.INVISIBLE);
                    nextButton.setVisibility(View.INVISIBLE);
                    //Execute after 2 Second
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            layout.setBackgroundResource(R.drawable.room_morning);
                            dialogTxt.setVisibility(View.VISIBLE);
                            dialogTxt.setCharacterDelay(50);
                            dialogTxt.animateText(dialog3);
                            nextActivity.setVisibility(View.VISIBLE);
                        }
                    }, 3000);
                } else {
                    textSwitcher.setText(dialog[++stringIndex]); //Else next Text from String
                }
                screenPopup();
            }
        });

        textSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                textView = new TextView(Home3Activity.this); //Create new TextView with the settings below
                textView.setTextColor(Color.WHITE);
                textView.setTextSize(15);
                textView.setPadding(50,20,20,20);
                return textView;
            }
        });
        textSwitcher.setText(dialog[stringIndex]); //Start Text

        playboy.play(this, R.raw.home);
        playboy.loop(true);

        nextActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playboy.stop();
                dialogTxt.setVisibility(View.INVISIBLE);
                nextActivity.setVisibility(View.INVISIBLE);
                playerSleep.play(Home3Activity.this,R.raw.goodnight);
                roomView.animate().alpha(0).setDuration(3000);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        playerSleep.stop();
                        Intent intent = new Intent(Home3Activity.this, School3Activity.class);// New activity
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish(); // Call once you redirect to another activity
                    }
                }, 9000);
            }
        });
    }

    public void screenPopup(){
        if(nightWithAiko){
            switch (stringIndex){
                case 1:
                    imageView.animate().alpha(1).setDuration(1000);
                    break;
                case 4:
                    imageView.setImageResource(R.drawable.aiko_formal_smile_blush);
                    moodView.setVisibility(View.VISIBLE);
                    break;
                case 7:
                    imageView.setImageResource(R.drawable.aiko_formal_smile);
                    moodView.setVisibility(View.INVISIBLE);
                    break;
                case 9:
                    imageView.animate().translationX(1300).setDuration(1000);
                    break;
            }
        } else if (nightWithMiki){
            switch (stringIndex){
                case 1:
                    imageView.setImageResource(R.drawable.aiko_formal_frown);
                    imageView.animate().alpha(1).setDuration(1000);
                    break;
                case 7:
                    imageView.setImageResource(R.drawable.aiko_formal_shout);
                    break;
                case 9:
                    imageView.setImageResource(R.drawable.aiko_formal_frown);
                    break;
                case 10:
                    imageView.animate().translationX(1300).setDuration(1000);
                    break;
            }
        }

    }
}
