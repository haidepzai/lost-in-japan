package com.example.japan;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.example.japan.Helper.BaseActivity;

public class AkihabaraNightActivity extends BaseActivity implements View.OnClickListener {

    private TextSwitcher textSwitcher; //Like TextView but with Fade in - Fade out for TextView
    private ImageButton nextButton;

    private int stringIndex = 0;
    private String[] dialog = {
            "Anime city: Akihabara",
            "You: What a crazy place here! These people, those music everywhere and all the stores! COOL \uD83D\uDE0E",
            "Aiko: Yeah that is Akihabara - a must visit place for Anime fans!!",
            "You: Tokyo definitely never sleeps at all. WOW!",
            "You: As someone who likes Anime like me, this place is like HEAVEN!",
            "Aiko: You look so happy ♥", //Case 5
            "You: I am in love with JAPAN!",
            "Aiko: So happy to hear that!! Hey " + LoginActivity.username + ", how about we go to a Game Center?",
            "You: Yes sure!",
            "Aiko: Here we go~", //Case 9
            "You: Wow so many claw machines!!! Okay which one should I try?",
            "Aiko: How about this one? There is a cute Pikachu plush!",
            "You: Okay lemme try my luck!! 100 yen per game... Let's try",
            "Aiko: GOOD LUCK!",
            "You: Okay... a bit to the right and then up and now grab it!!",
            "You: Yes yes yes come on!!",
            "Aiko: Ahhhh it fell down...",
            "You: Dammit.. I will try it again!",
            "Aiko: Haha be careful not to get addicted!!",
            "You: OMG again it lost the grip... I am sure this machine is designed to be unbeatable!",
            "Aiko: Probably haha but I can see your determination!",
            "You: Yeah I try it again!!",
            "*after tons of time and wasting tons of 100 yen coins*",
            "You: Gosh.. ok my last 100 yen coin, what shall I do?", //Case 23
            "Aiko: Wooooow nice!!! You did it!!",
            "You: Yessssssssss yatta!",
            "Aiko: Congrats!!",
            "Aiko: Hahaha I hope you had fun! \uD83D\uDE06",
            "You: Yeah I have!! Thanks!!!!!!",
            "Aiko: Okay it's getting late! Let's go home!"
    };

    private TextView textView;
    private ImageView imageView;
    private ImageView gameView;
    private ImageView plushView;

    private Button button1;
    private Button button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_akihabara_night);

        imageView = findViewById(R.id.imageAiko);
        gameView = findViewById(R.id.gameCenter);
        plushView = findViewById(R.id.imagePlush);

        button1 = findViewById(R.id.button);
        button2 = findViewById(R.id.button2);

        textSwitcher = findViewById(R.id.textSwitcher);
        nextButton = findViewById(R.id.imageButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){

                if (stringIndex == dialog.length-1){
                    playboy.stop();
                    Intent intent = new Intent(AkihabaraNightActivity.this, RoomActivity.class);// New activity
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish(); // Call once you redirect to another activity
                } else {
                    textSwitcher.setText(dialog[++stringIndex]); //Else next Text from String
                }
                screenPopup();
            }
        });
        //Fade in - Fade out for text in the textView
        textSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                textView = new TextView(AkihabaraNightActivity.this); //Create new TextView with the settings below
                textView.setTextColor(Color.WHITE);
                textView.setTextSize(15);
                textView.setPadding(50,20,20,20);
                return textView;
            }
        });
        textSwitcher.setText(dialog[stringIndex]); //Start Text
        //Music
        playboy.play(this, R.raw.akihabara);
        playboy.loop(true);

        button2.setOnClickListener(this);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                button1.setVisibility(View.INVISIBLE);
                button2.setVisibility(View.INVISIBLE);
                textSwitcher.setText("You: Failed again... I give up!");
                stringIndex = 26;
            }
        });
    }

    public void screenPopup(){

        switch(stringIndex){
            case 2:
                imageView.animate().alpha(1).setDuration(1000);
                break;
            case 5:
                imageView.setImageResource(R.drawable.aiko_smile_blush);
                break;
            case 9:
                imageView.setImageResource(R.drawable.aiko_smile_front);
                gameView.animate().alpha(1).setDuration(1000);
                break;
            case 23:
                button1.setVisibility(View.VISIBLE);
                button2.setVisibility(View.VISIBLE);
                nextButton.setVisibility(View.INVISIBLE);
                break;
            case 25:
                plushView.setVisibility(View.INVISIBLE);
                break;
            case 27:
                gameView.animate().alpha(0).setDuration(1000);
                imageView.setImageResource(R.drawable.aiko_smile_front);
                break;
        }

    }

    @Override
    public void onClick(View v) {
        plushView.setVisibility(View.VISIBLE);
        plushView.animate().scaleY(1).scaleX(1).setDuration(1000);
        nextButton.setVisibility(View.VISIBLE);
        button1.setVisibility(View.INVISIBLE);
        button2.setVisibility(View.INVISIBLE);
        imageView.setImageResource(R.drawable.aiko_shout_side);
        textSwitcher.setText("Ohhhhh I got it!!");
        stringIndex = 23;
    }
}
