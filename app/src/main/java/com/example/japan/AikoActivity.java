package com.example.japan;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.example.japan.Helper.BaseActivity;
import com.example.japan.Helper.MusicPlayer;
import com.example.japan.Helper.TypeWriter;

public class AikoActivity extends BaseActivity implements View.OnClickListener {

    MusicPlayer fireworkPlayer = new MusicPlayer();

    final Handler handler = new Handler(); //Handler for delayed execute

    private TextSwitcher textSwitcher; //Like TextView but with Fade in - Fade out for TextView
    private ImageButton nextButton;
    private TextView textView;

    private TypeWriter dialogTxt;
    private ImageButton nextActivity;

    private ImageView aikoView;
    private ImageView matsuriView;
    private ImageView hanabiView;
    private ImageView moodView;
    private ImageView chefView;
    private ImageView karaageView;

    private int stringIndex = 0;
    private String[] dialog = {
            "At the festival",
            "You: (Ohhh look at all these food stalls.. it's a paradise!)",
            "You: (But it is so crowded.. can barely move \uD83D\uDE05 )",
            "You: (Where's Aiko? She is a bit late...)",
            "Aiko: Heyyyyy!",
            "Aiko: Hello " + LoginActivity.username + "!!!!", //Case 5
            "You: Ah here you are!",
            "You: Wow you look fantastic in this Yukata!",
            "Aiko: Thanks ♥", //Case 8
            "You: So shall we walk around?",
            "Aiko: Yes ♥",
            "You: It smells so great here!", //Case 11
            "Aiko: Do you wanna try something?",
            "You: Yes can I?",
            "Aiko: うん！もちろん！(Yeah, of course!)",
            "Aiko: Let's go over there!",
            "*You and Aiko are enjoying the festival at its fullest, talking and laughing a lot*", //Case 16
            "You: Oh hey over there is the chef again!! Let's order there!",
            "Aiko: Ah I remember him! Okay!",
            "You: Hello!",
            "Soma: Oh hello!", //Case 20
            "You: Didn't expect to see you here!",
            "Soma: Hahahaha as I said I am everywhere! Do you wanna try my top secret Karaage?",
            "You: This looks so good! It's fried chicken, right?",
            "Aiko: Yes!",
            "You: Okay I'll have one!",
            "Soma: HERE YOU GO!", //Case 26
            "You: Ohhhhhhhhhh~ \uD83D\uDE0D",
            "You: ありがとうございます (Thank you so much)", //Case 28
            "You: Here Aiko, please try!",
            "Aiko: Thanks! ♥", //Case 30
            "Aiko: 暑いー！(Hot!!)",
            "You: Haha be careful!",
            "Aiko: SUPER DELICIOUS! \uD83D\uDE0D",
            "Soma: THANKS! Have fun you too! \uD83D\uDE0F",
            "You: Thank you! Keep up the good work!", //Case 34
            "Aiko: Thanks " + LoginActivity.username + " for spending this day with me...",
            "You: This is maybe the highlight of my trip! It's wonderful!",
            "Aiko: I hope you will never forget this day!",
            "You: Never! Thanks to you I had a really nice time! Best trip ever! Even though it was short..",
            "Aiko: We did the best of it! I'll be waiting for you in Japan!",
            "You: Thanks always for your kindness, Aiko",
            "Aiko: Thanks to you I feel relieved and happy... ",
            "Aiko: You know about Takuya... He really gave me so much pressure...", //Case 44
            "Aiko: At first I was super happy being with him but after the time I felt empty even though he treated me nicely...",
            "Aiko: He was such an overly attached boyfriend and his jealousy made everything worse...",
            "You: Sorry to hear so... I can feel you, Aiko",
            "Aiko: Thanks for listening to me " + LoginActivity.username + "! I feel much better!", //Case 48
            "Aiko: Oh the firework starts soon! We should watch!",
            "You: I can't wait to see Japanese firework!",
            "Aiko: Let's watch 花火 (Hanabi) together! ♥",
            "You: Hanabi means firework in Japanese, right?",
            "Aiko: Yes!",
            "Aiko: Oh here it goes!",
            "花火 (Hanabi) Firework", //Case 54
            "You: Wow... I am speechless... this is by far the most beautiful firework I've ever seen in my life!",
            "Aiko: I'm happy to spend this moment with you, " + LoginActivity.username, //Case 56
            "You: Me too.. Thanks for everything Aiko... I like you a lot",
            "Aiko: 私も " + LoginActivity.username + " くんが好きです (I like you too)", //Case 58
            "You: Thanks for today.. no thanks for all.. I had a wonderful time in Japan with you!",
            "Aiko: Let's enjoy this wonderful moment ♥",
            "*Both spend the whole night watching the firework*"

    };

    private String dialog2 = "What a wonderful finish... My trip could have not end better than this! Being here with " +
            "Aiko is like a dream! I can't thank her enough since the first day I arrived in Japan... She did a lot for me to " +
            "make my time in Japan amazing and unforgettable! She was the perfect host. I'm so happy to watch this beautiful firework " +
            "with her... it can't be more romantic!! Thank you Japan for these nice memories... I will come back for sure...";

    private Button button1;
    private Button button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aiko);

        aikoView = findViewById(R.id.yukataAiko);
        chefView = findViewById(R.id.imageChef);
        matsuriView = findViewById(R.id.matsuriImage);
        hanabiView = findViewById(R.id.hanabiImage);
        moodView = findViewById(R.id.imageMood);
        karaageView = findViewById(R.id.imageKaraage);

        button1 = findViewById(R.id.button);
        button2 = findViewById(R.id.button2);

        dialogTxt = findViewById(R.id.dialogText);
        nextActivity = findViewById(R.id.nextActivity);

        textSwitcher = findViewById(R.id.textSwitcher);
        nextButton = findViewById(R.id.imageButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (stringIndex == dialog.length - 1) {
                    nextButton.animate().alpha(0).setDuration(1000);
                    textSwitcher.animate().alpha(0).setDuration(1000);
                    aikoView.animate().alpha(0).setDuration(1000);
                    moodView.animate().alpha(0).setDuration(1000);
                    //Execute after 4 Second
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            fireworkPlayer.stop();
                            dialogTxt.setVisibility(View.VISIBLE);
                            dialogTxt.setCharacterDelay(50);
                            dialogTxt.animateText(dialog2);
                            nextActivity.setVisibility(View.VISIBLE);
                        }
                    }, 4000);

                } else {
                    textSwitcher.setText(dialog[++stringIndex]); //Else next Text from String
                }
                screenPopup();
            }
        });
        //Fade in - Fade out for text in the textView
        textSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                textView = new TextView(AikoActivity.this); //Create new TextView with the settings below
                textView.setTextColor(Color.WHITE);
                textView.setTextSize(15);
                textView.setPadding(50, 20, 20, 20);
                return textView;
            }
        });
        textSwitcher.setText(dialog[stringIndex]); //Start Text
        //Music
        playboy.play(this, R.raw.kimochi);
        playboy.loop(true);

        nextActivity.setOnClickListener(this);
    }

    public void screenPopup() {

        switch (stringIndex) {
            case 5:
            case 56:
                aikoView.animate().alpha(1).setDuration(1000);
                break;
            case 8:
            case 30:
            case 47:
            case 58:
                aikoView.setImageResource(R.drawable.aiko_yukata_blush);
                moodView.setImageResource(R.drawable.heart);
                moodView.setVisibility(View.VISIBLE);
                break;
            case 11:
            case 31:
            case 48:
                aikoView.setImageResource(R.drawable.aiko_yukata_smile);
                moodView.setVisibility(View.INVISIBLE);
                break;
            case 16:
                matsuriView.animate().alpha(1).setDuration(1000);
                break;
            case 20:
                chefView.animate().alpha(1).setDuration(1000);
                break;
            case 26:
                karaageView.animate().alpha(1).setDuration(1000);
                break;
            case 28:
                karaageView.animate().alpha(0).setDuration(1000);
                break;
            case 35:
                chefView.animate().alpha(0).setDuration(1000);
                break;
            case 43:
                aikoView.setImageResource(R.drawable.aiko_yukata_frown);
                moodView.setImageResource(R.drawable.tear);
                moodView.setVisibility(View.VISIBLE);
                break;
            case 54:
                playboy.stop();
                playboy.play(AikoActivity.this, R.raw.intro);
                playboy.loop(true);
                fireworkPlayer.play(AikoActivity.this, R.raw.firework);
                fireworkPlayer.loop(true);
                hanabiView.animate().alpha(1).setDuration(1000);
                aikoView.animate().alpha(0).setDuration(1000);
                break;

        }

    }

    @Override
    public void onClick(View v) {
        playboy.stop();
        Intent intent = new Intent(AikoActivity.this, EndActivity.class);// New activity
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish(); // Call once you redirect to another activity
    }
}
