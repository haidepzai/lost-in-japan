package com.example.japan;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.example.japan.Helper.BaseActivity;

public class RestaurantActivity extends BaseActivity {

    private TextSwitcher textSwitcher; //Like TextView but with Fade in - Fade out for TextView
    private ImageButton nextButton;

    private boolean isDinnerPaid = true;

    private int stringIndex = 0;
    private String[] dialog = {
            "A fancy restaurant",
            "You: Wow, what a fancy restaurant! Thanks for showing me this luxurious restaurant, Aiko",
            "Aiko: You are welcome! I hope you like it",
            "You: Awesome!! I'm so fucking hungry and damn this relaxing piano music is superb!!",
            "Aiko: I am so glad you like it, " + LoginActivity.username,
            "You: So have you decided what you will take?",
            "Aiko: I will take Gyu-Don! It's a rice bowl with thin beef slices!!",
            "You: Cool! Any recommendation?",
            "Aiko: Hmmmm how about Tonkotsu Ramen? I think you should try it!!",
            "You: Sounds great! I'll take it!",
            "Chef: Hey yoooo welcome! What can I cook for you today?",
            "You: I'll take the Tonkotsu Ramen and for her the Gyu-Don please",
            "Chef: Alright!!",
            "You: Can't wait to have my first meal in Japan \uD83D\uDE0D",
            "You: So what is your favorite dish, Aiko?",
            "Aiko: Hmmm it's Okonomiyaki! If you like we can go there tomorrow!",
            "You: Sure, why not!",
            "Aiko: Hehe I can see your happiness!",
            "You: You are really kind, Aiko! Thanks a lot!",
            "Aiko: Ahahaha it's nothing! You are very welcome \uD83E\uDD70",
            "Chef: Sorry for the wait!!",
            "Chef: One Gyu-Don for the lady and the premium Tonkotsu Ramen for the gentleman! Enjoy!!",
            "You: WOOOOW THIS LOOKS SO GOOD! \uD83E\uDD24", //Case: 22
            "Aiko: いただきます (Itadakimasu - Before you start to eat, you say this phrase)", //Case: 23
            "You: OMG This is soooooo delicious!",
            "Aiko: Yay!!",//25
            "*some minutes later*",
            "You: Oh yeah... That was super good!! I am so full now... it was delicious",
            "Aiko: Thanks goodness hehe", //28
            "You: It was soooo nice, really!",
            "Aiko: So what do you like about Japan?", //30
            "You: Well it's a fantastic country with nice people, beautiful culture and delicious food..." +
                    "It was always a dream for me to come here and finally it's true!",
            "Aiko: Wow I can feel you really like Japan! I am proud of it!",
            "You: Of course you can!! I can't wait to see my new school! Too bad we are not in the same class...",
            "Aiko: Haha yeah but you will still find some good friends, I'm sure about it!",
            "You: I hope so!! \uD83D\uDE0A",
            "Aiko: So shall we go home?", //36
            "You: Yup let's go!", //37
            "You: Let me also pay for you!!", //If choose 1
            "Aiko: Oh noooo you don't have to!", //39
            "You: It's ok!! It's like a thank you present for guiding me!",
            "Aiko: Ahhhh~ Okay!! Thank you, " + LoginActivity.username, //Case 41
            "You: You are welcome! Ok let's GO!"
    };
    private String dialog2 = "Aiko: Alright let's go back home!";

    private TextView textView;
    private ImageView imageView;
    private ImageView chefView;
    private ImageView ramenView;
    private ImageView heartView;

    private Button button1;
    private Button button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);
        //After leaving the restaurant and come back home AGAIN
        //Only occurs when player did NOT choose to go to restaurant at the beginning
        //If not have visited Restaurant at beginning, set come Back Home to true
        if(!HomeActivity.visitedRestaurant){
            HomeActivity.comeBackHome = true;
        }

        playboy.play(this, R.raw.jazz);
        playboy.loop(true);

        button1 = findViewById(R.id.button);
        button2 = findViewById(R.id.button2);
        imageView = findViewById(R.id.imageAiko);
        chefView = findViewById(R.id.imageChef);
        ramenView = findViewById(R.id.imageRamen);
        heartView = findViewById(R.id.imageMood);
        textSwitcher = findViewById(R.id.textSwitcher);

        nextButton = findViewById(R.id.imageButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){

                if (stringIndex == dialog.length-1 || !isDinnerPaid){ //If last Index is reached or Player didn't pay for dinner: Go home
                    playboy.stop();
                    Intent intent = new Intent(RestaurantActivity.this, HomeActivity.class);// New activity
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish(); // Call once you redirect to another activity
                } else {
                    textSwitcher.setText(dialog[++stringIndex]); //Else next Text from String
                }
                screenPopup();
            }
        });
        //Fade in - Fade out for text in the textView
        textSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                textView = new TextView(RestaurantActivity.this); //Create new TextView with the settings below
                textView.setTextColor(Color.WHITE);
                textView.setTextSize(15);
                textView.setPadding(50,20,20,20);
                return textView;
            }
        });
        textSwitcher.setText(dialog[stringIndex]); //Start Text

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                stringIndex = 39;
                textSwitcher.setText(dialog[stringIndex]);
                button1.setVisibility(View.INVISIBLE);
                button2.setVisibility(View.INVISIBLE);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                textSwitcher.setText(dialog2);
                button1.setVisibility(View.INVISIBLE);
                button2.setVisibility(View.INVISIBLE);
                isDinnerPaid = false;
            }
        });

    }

    public void screenPopup(){

        switch(stringIndex){
            case 2:
                imageView.animate().alpha(1).setDuration(1000);
                break;
            case 10:
                chefView.setVisibility(View.VISIBLE);
                chefView.animate().translationX(0).setDuration(1000);
                break;
            case 13:
                chefView.animate().translationXBy(-1300).setDuration(1000);
                break;
            case 16:
            case 25:
                imageView.setImageResource(R.drawable.aiko_front_shout);
                break;
            case 19:
                imageView.setImageResource(R.drawable.aiko_front_shout_blush);
                break;
            case 20:
                imageView.setImageResource(R.drawable.aiko_front_smile);
                chefView.setVisibility(View.VISIBLE);
                chefView.animate().translationX(0).setDuration(1000);
                break;
            case 22:
                chefView.animate().translationXBy(-1300).setDuration(1000);
                ramenView.setVisibility(View.VISIBLE);
                ramenView.animate().scaleX(1).scaleY(1).setDuration(1000);
                break;
            case 23:
                ramenView.setVisibility(View.INVISIBLE);
                break;
            case 30:
                imageView.setImageResource(R.drawable.aiko_winter_smile);
                break;
            case 36:
                imageView.setImageResource(R.drawable.aiko_front_smile);
                button1.setVisibility(View.VISIBLE);
                button2.setVisibility(View.VISIBLE);
                nextButton.setVisibility(View.INVISIBLE);
                break;
            case 39:
                imageView.setImageResource(R.drawable.aiko_front_yell);
                break;
            case 41:
                imageView.setImageResource(R.drawable.aiko_front_yell_blush);
                heartView.setVisibility(View.VISIBLE);
                break;
        }
    }
}
