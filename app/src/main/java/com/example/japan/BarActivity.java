package com.example.japan;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.example.japan.Helper.BaseActivity;

public class BarActivity extends BaseActivity {

    final Handler handler = new Handler(); //For delayed execute

    private TextSwitcher textSwitcher; //Like TextView but with Fade in - Fade out for TextView
    private ImageButton nextButton;

    private int stringIndex = 0;
    private String[] dialog = {
            "Somewhere in Shinjuku",
            "You: (Even at night there are so many people on the street!)",
            "You: (Tokyo's nightlife is one of the best! Woah!)",
            "You: (So this place is famous for lots of bars... There are a lot of bar alleys here, cool!)",
            "You: (Everyone is having fun!!)",
            "You: (Alright..)",
            "You: (So this must be the bar Miki told me about)",
            "You: (An Izakaya huh? A Japanese pub..can't wait to eat typical Izakaya food and drink beer)",
            "You: (Okay I should go inside!)",
            "You: (So where is Miki?)", //Case 9
            "Miki: Hey " + LoginActivity.username + " over here!",
            "You: Ohhhh hi!",
            "Miki: Thanks for coming! ♥ How are you?",
            "You: Very well! Cool place here! I like this atmosphere!",
            "Miki: Many Japanese like to go to drink after work!\nYou can see a lot of drunk people here having fun!",
            "You: Yeaaaaah I love it!",
            "Miki: So are you hungry? Shall we order?",
            "You: Sure! What can you recommend?",
            "Miki: Let's take Yakitori!",
            "You: Ahhhh like grilled chicken on a skewer?",
            "Miki: Yup!!",
            "Miki: すみません！(Excuse me)",
            "Chef: Welcome!! Yes please?", //Case 22
            "You: Oh wait you again!",
            "Chef: Ahahaha I'm EVERYWHERE!",
            "You: Lol you are funny \uD83E\uDD23",
            "Miki: Hahaha you seem to know him well \uD83E\uDD23",
            "Miki: We would like to have different kind of Yakitori please",
            "Chef: Got it! Just a moment!",
            "You: Hahahaha it's so funny", //Case 29
            "Chef: Here you go!",
            "You: You are so fast!",
            "You: Woooooow", //Case 32
            "Chef: Enjoy!!",
            "You: Thank you!!", //Case 34
            "Miki: It's always so funny hanging out with you!",
            "You: Yeaaaah I cannot thank you enough! I miss the Onigiri you made for me\nThey were super delicious!!",
            "Miki: Oh really? Thanks ♥", //Case 37
            "You: You are a good cook!!!",
            "Miki: I can make for you again of course!",
            "You: Really?",
            "Miki: Of course!!", //Case 41
            "You: Oh these Yakitori are nice!!!",
            "Miki: うん！美味しいですね (Delicious isn't it?)",
            "You: はい！(Yes)",
            "Miki: So how's life in your hometown?",
            "You: My hometown mhhh it's a good place to live actually! Not so lively like Tokyo but it's nice!\nYou should come someday!",
            "Miki: I want to!! Have you ever planned to come back to Japan?",
            "You: Hmm actually no... I hope I can come soon again....",
            "Miki: We have to meet again for sure!",
            "You: Of course! It's a promise!",
            "Miki: Hey " + LoginActivity.username + " have you ever been in a situation where you like someone " +
                    "but you are not sure what the other person thinks?", //Case 51
            "Miki: Huh?!",
            "You: It is me, right?",
            "Miki: mhh mhhh", //Case 54
            "Miki: ....",
            "Miki: うん。すき(Yes, I like you)",
            "You: Miki...",
            "You: Hahaha you are cute",
            "You: I really like you too and love it to hang out with you!!",
            "Miki: But you don't feel the same?",
            "You: I do feel the same, Miki! I also like you a lot!!\nWould y---",
            "Miki: Ahhhhhhh the day after tomorrow is the firework festival!!\nWe can go there if you want!!", //case 62
            "You: (She just interrupted me) \uD83D\uDE05",
            "You: Ahhh yeah!! That sounds nice! I always wanted to see Japanese firework",
            "Miki: It's a big お祭り (festival) if you like to go, call me please!",
            "You: Yeah I will!",
            "Miki: Alright! Then see you tomorrow in school!!",
            "You: Yup!! Good night, Miki!",
            "Miki: Good night, " + LoginActivity.username

    };

    private boolean likeMe = true;
    private int stringIndex2 = -1;
    private String[] dialog2 = {
            "Miki: Well yeah but I dunno what he thinks...",
            "You: That's a difficult situation.. you have to ask him, otherwise you will never know",
            "Miki: Yes, you are right!!",
            "You: You can do it!! Believe in yourself, Miki!!",
            "Miki: Ahaha thank you!! You are so kind ♥",
            "You: いえいえ！(No no it's nothing)",
    };

    private TextView textView;

    private ImageView mikiView;
    private ImageView barView;
    private ImageView chefView;
    private ImageView yakitoriView;
    private ImageView moodView;

    private Button button1;
    private Button button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar);

        button1 = findViewById(R.id.button);
        button2 = findViewById(R.id.button2);

        mikiView = findViewById(R.id.imageMiki);
        barView = findViewById(R.id.barImage);
        chefView = findViewById(R.id.imageChef);
        yakitoriView = findViewById(R.id.imageYakitori);
        moodView = findViewById(R.id.imageMood);

        textSwitcher = findViewById(R.id.textSwitcher);

        nextButton = findViewById(R.id.imageButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                //dialog1
                if(likeMe) {
                    if (stringIndex == dialog.length-1){
                        playboy.stop();
                        Intent intent = new Intent(BarActivity.this, Home3Activity.class);// New activity
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish(); // Call once you redirect to another activity
                    } else {
                        textSwitcher.setText(dialog[++stringIndex]); //Else next Text from String
                    } //dialog2
                } else if (stringIndex2 == dialog2.length-1){
                    likeMe = true;
                    stringIndex = 61;
                    moodView.setVisibility(View.INVISIBLE);
                    textSwitcher.setText("You: But you know... you are really sweet and I li-----");
                } else {
                    textSwitcher.setText(dialog2[++stringIndex2]);
                }
                screenPopup();
            }
        });
        //Fade in - Fade out for text in the textView
        textSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                textView = new TextView(BarActivity.this); //Create new TextView with the settings below
                textView.setTextColor(Color.WHITE);
                textView.setTextSize(15);
                textView.setPadding(50,20,20,20);
                return textView;
            }
        });
        textSwitcher.setText(dialog[stringIndex]); //Start Text
        //Music
        playboy.play(this, R.raw.bar);
        playboy.loop(true);
        //Me?
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                button1.setVisibility(View.INVISIBLE);
                button2.setVisibility(View.INVISIBLE);
                textSwitcher.setText("You: Hmmm is that person maybe.. me?");
                mikiView.setImageResource(R.drawable.miki_pose2_winter_open_blush);
            }
        });
        //I know that feel
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                button1.setVisibility(View.INVISIBLE);
                button2.setVisibility(View.INVISIBLE);
                textSwitcher.setText("You: Ah yeah it sucks....Is there someone you like?");
                likeMe = false;
            }
        });
    }

    public void screenPopup(){

        if(likeMe){
            switch(stringIndex){
                case 9:
                    barView.animate().alpha(1).setDuration(1000);
                    break;
                case 11:
                    mikiView.animate().alpha(1).setDuration(1000);
                    break;
                case 22:
                case 30:
                    chefView.animate().translationX(0).setDuration(1000);
                    break;
                case 29:
                case 34:
                    chefView.animate().translationX(-1300).setDuration(1000);
                    break;
                case 32:
                    yakitoriView.animate().alpha(1).setDuration(1000);
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            yakitoriView.animate().alpha(0).setDuration(1000);
                        }
                    }, 5000);
                    break;
                case 37:
                    mikiView.setImageResource(R.drawable.miki_pose4_winter_shout_blush);
                    moodView.setVisibility(View.VISIBLE);
                    break;
                case 41:
                    mikiView.setImageResource(R.drawable.miki_pose4_winter_smile);
                    moodView.setVisibility(View.INVISIBLE);
                    break;
                case 51:
                    button1.setVisibility(View.VISIBLE);
                    button2.setVisibility(View.VISIBLE);
                    nextButton.setVisibility(View.INVISIBLE);
                    break;
                case 54:
                    mikiView.setImageResource(R.drawable.miki_pose3_winter_frown_blush);
                    break;
                case 62:
                    mikiView.setImageResource(R.drawable.miki_pose1_winter_shout);
                    break;
            }
        } else {
            switch (stringIndex2){
                case 4:
                    mikiView.setImageResource(R.drawable.miki_pose1_winter_smile_blush);
                    moodView.setVisibility(View.VISIBLE);
                    break;
            }
        }

    }
}
