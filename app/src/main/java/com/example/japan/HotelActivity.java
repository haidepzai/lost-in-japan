package com.example.japan;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.example.japan.Helper.BaseActivity;
import com.example.japan.Helper.MusicPlayer;
import com.example.japan.Helper.TypeWriter;

public class HotelActivity extends BaseActivity implements View.OnClickListener {

    final Handler handler = new Handler(); //Handler for delayed execute

    private MusicPlayer ringtone = new MusicPlayer();

    private TextView textView;
    private TextSwitcher textSwitcher; //Like TextView but with Fade in - Fade out for TextView
    private ImageButton nextButton;

    private TypeWriter dialogTxt;
    private ImageButton nextActivity;

    private ImageView hotelView;
    private ImageView iphoneView;

    private int stringIndex = 0;
    private String[] dialog = {
            "*You left Aiko's house*",
            "You: (Damn it was so heartbreaking....)",
            "You: (It was so hard to tell her this...)",
            "You: (Dunno if this was the right decision but I wanted to see Miki)",
            "You: (But it is late... I should check in at a hotel first...)",
            "*You decided to stay at a hotel*",
            "You: Okay...", //Case 6
            "You: (It feels so cold and empty here...)",
            "You: (I still feel guilty, dammit!!!)",
            "You: Huh?", //Case 9
            "You: Hello?", //10
            "Miki: Hey it's me!",
            "You: Oh hey Miki!!!! Why didn't you come to school today? Are you ok?",
            "Miki: Yes... I'm okay.. I just didn't feel well... My body is not so strong though.. Sorry for making you worry..",
            "You: Yeah me and our Sensei are worried!! Are you really ok?",
            "Miki: Yes! Thanks! Feeling so much better now!! I just wanted to ask about tomorrow...",
            "You: Oh yes! So would you like to go out with me?",
            "Miki: ....",
            "Miki: YES! ♥",
            "You: Alright! Let's meet tomorrow and enjoy the festival!",
            "Miki: うん！(Yeah)",
            "You: You should rest... Have a good night, Miki!",
            "Miki: Thank you " + LoginActivity.username + ", you too!! おやすみ (Good night)",
            "You: Good night, Miki",
    };

    private String dialog2 = "What a though day... I still feel very bad because of Aiko... " +
            "She did a lot for me and it broke my heart seeing her like that but I think I made the right decision... " +
            "In such a case there will always be someone who gets hurt... Life goes on, I hope she will be fine soon... " +
            "Anyway there was no other option.. either way, one of them will be sad... I will do my best to make Miki happy! " +
            "It's time to sleep...";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel);

        hotelView = findViewById(R.id.hotelView);
        iphoneView = findViewById(R.id.imageiPhone);

        dialogTxt = findViewById(R.id.dialogText);
        nextActivity = findViewById(R.id.nextActivity);

        textSwitcher = findViewById(R.id.textSwitcher);
        nextButton = findViewById(R.id.imageButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                if (stringIndex == dialog.length-1){
                    textSwitcher.setVisibility(View.INVISIBLE);
                    nextButton.setVisibility(View.INVISIBLE);
                    handler.postDelayed(new Runnable() { //Execute after 2 secons
                        @Override
                        public void run() {
                            dialogTxt.setVisibility(View.VISIBLE);
                            dialogTxt.setCharacterDelay(50);
                            dialogTxt.animateText(dialog2);
                            nextActivity.setVisibility(View.VISIBLE);
                        }
                    }, 2000);
                } else {
                    textSwitcher.setText(dialog[++stringIndex]); //Else next Text from String
                }
                screenPopup();
            }
        });
        //Fade in - Fade out for text in the textView
        textSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                textView = new TextView(HotelActivity.this); //Create new TextView with the settings below
                textView.setTextColor(Color.WHITE);
                textView.setTextSize(15);
                textView.setPadding(50,20,20,20);
                return textView;
            }
        });
        textSwitcher.setText(dialog[stringIndex]); //Start Text

        nextActivity.setOnClickListener(this);

    }

    public void screenPopup(){

        switch (stringIndex){
            case 6:
                playboy.play(HotelActivity.this, R.raw.sadsong);
                playboy.loop(true);
                hotelView.animate().alpha(1).setDuration(1000);
                break;
            case 9:
                ringtone.play(HotelActivity.this, R.raw.ringtone);
                iphoneView.setVisibility(View.VISIBLE);
                iphoneView.animate().scaleY(1).scaleX(1).setDuration(1000);
                break;
            case 10:
                iphoneView.setVisibility(View.INVISIBLE);
                ringtone.stop();
                break;
        }

    }

    @Override
    public void onClick(View v) {
        playboy.stop();
        Intent intent = new Intent(HotelActivity.this, TrainActivity.class);// New activity
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish(); // Call once you redirect to another activity
    }
}
