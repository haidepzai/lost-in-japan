package com.example.japan;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.japan.Helper.BaseActivity;
import com.example.japan.Helper.MusicPlayer;
import com.example.japan.Helper.TypeWriter;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    MusicPlayer airPlayer = new MusicPlayer();

    private EditText inputName;

    private Button playBTN;

    private ImageView planeImg;

    private TypeWriter introTxt;
    private ImageButton nextBtn;

    public static String username;
    private String startText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        playboy.play(LoginActivity.this, R.raw.intro);
        playboy.loop(true);

        inputName = findViewById(R.id.inputName);
        playBTN = findViewById(R.id.playButton);
        nextBtn = findViewById(R.id.nextButton);

        planeImg = findViewById(R.id.airplane);
        introTxt = findViewById(R.id.introText);

        playBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playboy.stop();
                username = inputName.getText().toString();
                startText = "My name is " + username + " and this will be my first trip to Japan. " +
                        "I'm so excited that I couldn't sleep at all last night!! This will be great! I can feel it. " +
                        "I am so looking forward to my new life as an exchange student even though it's just for a week...\n " +
                        "But I still hope to find some good friends there, make some good memories and have a good time... " +
                        "Finally a dream will come true!! I can't wait to arrive at Tokyo Haneda airport OMG!\n " +
                        "This will be a long flight... Guess I'll take a little rest...";
                if (username.equals("")) {
                    Toast.makeText(LoginActivity.this, "Please enter your name", Toast.LENGTH_SHORT).show();
                } else {
                    airPlayer.play(LoginActivity.this, R.raw.airplane);
                    inputName.setVisibility(View.INVISIBLE);
                    playBTN.setVisibility(View.INVISIBLE);
                    planeImg.animate().alpha(1).setDuration(2000); //Image will slowly appear
                    //Execute following after a 3 Secs
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            introTxt.setVisibility(View.VISIBLE);
                            introTxt.setCharacterDelay(50);
                            introTxt.animateText(startText);
                            nextBtn.setVisibility(View.VISIBLE);
                        }
                    }, 3000);
                }
            }
        });
        nextBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        airPlayer.stop();
        Intent intent = new Intent(this, MainActivity.class);// New activity
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish(); // Call once you redirect to another activity
    }
}
