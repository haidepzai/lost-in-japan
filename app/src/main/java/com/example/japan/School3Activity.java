package com.example.japan;

import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.example.japan.Helper.BaseActivity;
import com.example.japan.Helper.MusicPlayer;

public class School3Activity extends BaseActivity {
    private ConstraintLayout layout; //For changing background image later

    private MusicPlayer bellPlayer = new MusicPlayer();

    public static int counter = 0;

    private TextView textView;
    private TextSwitcher textSwitcher; //Like TextView but with Fade in - Fade out for TextView
    private ImageButton nextButton;

    private ImageView teacherView;
    private ImageView housingView;
    private ImageView answerView;
    private ImageView aikoView;
    private ImageView moodView;

    private Button button1;
    private Button button2;
    private Button button3;
    private Button button4;

    public static boolean dateMiki = false;
    public static boolean dateAiko = false;

    private int stringIndex = 0;
    private String[] dialog = {
            "Last day in school",
            "You: (This week passed so quickly... already last day in school!)",
            "You: (Where's Miki? She is not here.. is she sick?)",
            "You: (Maybe I should visit her later..)",
            "Watanabe-Sensei: Alright listen up!",
            "Watanabe-Sensei: Soseki Natsume was a very popular Japanese novelist. He translated the English phrase for " +
                    "'I love you' in Japanese. " + LoginActivity.username + "! Do you know translation into Japanese?", //Case 5
            "You: That was an interesting question!",
            "You: (Oh still half an hour...)",
            "You: ....",
            "You: ....",
            "You: I will miss this school...",
            "Watanabe-Sensei: Okay that's it for today!", //Case 11
            "Watanabe-Sensei: " + LoginActivity.username + "? Do you have a moment?",
            "You: Yes! What is it Sensei?",
            "", //Case 14
            "You: Thanks for the result, I will do my best from now on!",
            "Watanabe-Sensei: Anyway... it's about Miki, you are her friend, right? I'm a bit worried about her absent.. " +
                    "Could you see after her?",
            "You: Yeah I'm worried as well... I'm not sure if my time schedule allows me but I try my best!",
            "Watanabe-Sensei: Thank you " + LoginActivity.username + "! This is your last day right?\n" +
                    "I hope you had fun and could learn some Japanese history.",
            "You: Thanks Sensei!",
            "Watanabe-Sensei: Take care!", //Case 20
            "You: Alright... time to go home...",
            "On your way home", //Case 22
            "You: (Okay almost there!)",
            "You: (Ah damn tomorrow is the festival already...)",
            "You: (I should decide now with whom I should go to the festival..)",
            "Aiko or Miki? Choose wisely!!", //Case 26
            "You: Okay... time to go!!"
    };

    private int stringIndex2 = -1;
    private String[] dialog2 = {
            "Aiko's House",
            "You: Aiko?",
            "Aiko: Hey " + LoginActivity.username + " welcome home!",
            "You: Thanks Aiko! I have to tell you something...",
            "Aiko: Mhhh? What is it?",
            "You: It's about... I can't stay here anymore...",
            "Aiko: What? Why? \uD83D\uDE30", //Case 6.2
            "You: I have thought of many things... Aiko... Thank you for everything...",
            "Aiko: Why so sudden?",
            "You: It's not your fault, Aiko.. it's just... I...",
            "Aiko: ?",
            "Aiko: Is it because of Takuya??",
            "You: Not at all...",
            "Aiko: Tell me please!!!!",
            "You: I've decided to spend Matsuri with someone else...",
            "Aiko: ...............", //Case 15
            "You: So sorry Aiko... I really like you too but there is no room for more..",
            "Aiko: I feel betrayed..",
            "You: Please understand me.. I really appreciate a lot that you spent time with me, " +
                    "showing me around and so on but there is someone I really wanna take care of..",
            "Aiko: I see..",
            "You: Aiko...",
            "Aiko: Don't worry about me.. I'm fine... so then that's the goodbye, right?",
            "You: .....",
            "Aiko: I understand... Please take care, " + LoginActivity.username,
            "Aiko: さようなら (Sayonara)"

    };

    private boolean endDialog2 = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school3);
        Log.i("Info", "Counter: " + counter);

        layout = findViewById(R.id.background);

        teacherView = findViewById(R.id.imageTeacher);
        housingView = findViewById(R.id.housing);
        answerView = findViewById(R.id.imageAnswer);
        aikoView = findViewById(R.id.imageAiko);
        moodView = findViewById(R.id.imageMood);

        button1 = findViewById(R.id.button);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        button4 = findViewById(R.id.button4);

        textSwitcher = findViewById(R.id.textSwitcher);
        nextButton = findViewById(R.id.imageButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){


                if (stringIndex == dialog.length-1){
                    if(endDialog2){
                        playboy.stop();
                        Intent intent = new Intent(School3Activity.this, HotelActivity.class);// New activity
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish(); // Call once you redirect to another activity
                    }
                    if(dateMiki){
                        dialog = dialog2;
                        stringIndex = stringIndex2;
                        layout.setBackgroundResource(R.drawable.home);
                        playboy.stop();
                        textSwitcher.setText("You: I have to tell Aiko before...");
                    } else if(dateAiko) {
                        playboy.stop();
                        Intent intent = new Intent(School3Activity.this, MikiHouseActivity.class);// New activity
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish(); // Call once you redirect to another activity
                    }

                } else {
                    textSwitcher.setText(dialog[++stringIndex]); //Else next Text from String
                }
                screenPopup();
            }
        });
        //Fade in - Fade out for text in the textView
        textSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                textView = new TextView(School3Activity.this); //Create new TextView with the settings below
                textView.setTextColor(Color.WHITE);
                textView.setTextSize(15);
                textView.setPadding(50,20,20,20);
                return textView;
            }
        });
        textSwitcher.setText(dialog[stringIndex]); //Start Text
        //Music
        playboy.play(this, R.raw.school);
        playboy.loop(true);

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                button3.setVisibility(View.INVISIBLE);
                button4.setVisibility(View.INVISIBLE);
                textSwitcher.setText("Watanabe-Sensei: Wrong! The correct phrase is 月が綺麗ですね which literally means\n" +
                                "'The moon is beautiful, isn't it?'");
                answerView.setImageResource(R.drawable.wrong);
                answerView.setVisibility(View.VISIBLE);
            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                button3.setVisibility(View.INVISIBLE);
                button4.setVisibility(View.INVISIBLE);
                textSwitcher.setText("Watanabe-Sensei: Correct! The Japanese phrase is 月が綺麗ですね which literally means\n" +
                                "'The moon is beautiful, isn't it?'");
                answerView.setVisibility(View.VISIBLE);
                counter++;
                Log.i("Info", "Counter: " + counter);
            }
        });

        //Go with Miki
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                button1.setVisibility(View.INVISIBLE);
                button2.setVisibility(View.INVISIBLE);
                textSwitcher.setText("You: Miki is really a sweetheart... and I'm kinda worried of her...\n" +
                        "I will go with her this time... Sorry Aiko... I have to tell her that..");
                dateMiki = true;

            }
        });
        //Go with Aiko
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                button1.setVisibility(View.INVISIBLE);
                button2.setVisibility(View.INVISIBLE);
                textSwitcher.setText("You: I really like Miki... But.. Aiko is my host... I have to go with her!!\n" +
                        "Sorry Miki... I have to tell her that..");
                dateAiko = true;

            }
        });
    }

    public void screenPopup(){

        if(!dateAiko && !dateMiki) {
            switch(stringIndex){
                case 4:
                    teacherView.animate().translationX(0).setDuration(1000);
                    break;
                case 5:
                    nextButton.setVisibility(View.INVISIBLE);
                    button3.setVisibility(View.VISIBLE);
                    button4.setVisibility(View.VISIBLE);
                    break;
                case 6:
                    teacherView.animate().translationX(-1300).setDuration(1000);
                    answerView.setVisibility(View.INVISIBLE);
                    break;
                case 11:
                    teacherView.animate().translationX(0).setDuration(1000);
                    bellPlayer.play(School3Activity.this, R.raw.bell);
                    break;
                case 14:
                    textSwitcher.setText("Watanabe-Sensei: It's your last day, right? I wanna give you the result...\n" +
                            "You have " + counter + " of 3 questions correct!");
                    bellPlayer.stop();
                    break;
                case 20:
                    teacherView.animate().translationX(-1300).setDuration(1000);
                    break;
                case 22:
                    housingView.animate().alpha(1).setDuration(1000);
                    break;
                case 26:
                    nextButton.setVisibility(View.INVISIBLE);
                    button1.setVisibility(View.VISIBLE);
                    button2.setVisibility(View.VISIBLE);
                    break;
            }
        } else if (dateMiki){
            switch(stringIndex){
                case 0:
                    playboy.play(School3Activity.this,R.raw.home);
                    housingView.animate().alpha(0).setDuration(1000);
                    break;
                case 2:
                    aikoView.animate().alpha(1).setDuration(1000);
                    break;
                case 6:
                    aikoView.setImageResource(R.drawable.aiko_winter_shout);
                    playboy.stop();
                    break;
                case 7:
                    playboy.play(School3Activity.this, R.raw.kokoro_to_kokoro);
                    playboy.loop(true);
                    break;
                case 8:
                    aikoView.setImageResource(R.drawable.aiko_winter_frown);
                    break;
                case 15:
                    moodView.setVisibility(View.VISIBLE);
                    break;
                case 24:
                    endDialog2 = true;
                    break;

            }
        }


    }
}
