package com.example.japan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.example.japan.Helper.BaseActivity;
import com.example.japan.Helper.MusicPlayer;

public class MainActivity extends BaseActivity {
    //MediaPlayer player1;

    private TextSwitcher textSwitcher; //Like TextView but with Fade in - Fade out for TextView
    private ImageButton nextButton;

    private int stringIndex = 0;
    private String[] dialog = {
            "Tokyo Haneda International Airport",
            "You: .....",
            "You: (I'm here.. I can't believe it!!!)",
            "You: THIS IS TRUE!!",
            "You: (Wow finally I am in Japan! Damn this is not a dream!! Hell yeah!)",
            "You: (I can't wait to have a good time and make some nice memories!!)",
            "You: (I bet my friends in my hometown are jealous by now haha)",
            "You: (Okay, I should look for my host.. According to the mail, her name is Aiko..)",
            "You: (Where could she be? Is this the wrong gate?)",
            "You: (Okay.. what should I do now?!)",
            "Someone: HEY!!!!",
            "Girl: Oh hey! You must be the new exchange student? Welcome to Japan!",
            "You: Oh Hey!!! Nice to meet you! I am " + LoginActivity.username,
            "Aiko: Hey " + LoginActivity.username + "!! I am Aiko and I will be your host!",
            "Aiko: Hope you will have a great time in Japan!",
            "Aiko: If you have any questions, please ask me anytime!",
            "Aiko: I'll be looking forward to guiding you ♥",
            "You: Thanks a lot!! You are so kind Aiko, ありがとう (Thanks)!!",
            "You: You know, it's like a dream came true!!",
            "Aiko: I am happy to hear so!! \uD83D\uDE01",
            "You: I can't wait to have a good time! Couldn't sleep at all on the plane but I'm soooo excited!!",
            "Aiko: Oh.. I bet you are tired then... Shall we go home first?",
            "Aiko: Alright! Let's go!!"
    };

    private TextView textView;
    private ImageView imageView;

    private Button button1;
    private Button button2;

    private boolean goHome = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button1 = findViewById(R.id.button);
        button2 = findViewById(R.id.button2);
        imageView = findViewById(R.id.imageAiko);
        textSwitcher = findViewById(R.id.textSwitcher);
        nextButton = findViewById(R.id.imageButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                if (stringIndex == dialog.length-1){
                    if(goHome){
                        playboy.stop();
                        Intent intent = new Intent(MainActivity.this, HomeActivity.class);// New activity
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish(); // Call once you redirect to another activity
                    } else {
                        playboy.stop();
                        HomeActivity.visitedRestaurant = true;
                        Intent intent = new Intent(MainActivity.this, RestaurantActivity.class);// New activity
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish(); // Call once you redirect to another activity
                    }
                } else {
                    textSwitcher.setText(dialog[++stringIndex]); //Else next Text from String
                }
                screenPopup();
            }
        });
        //Fade in - Fade out for text in the textView
        textSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                textView = new TextView(MainActivity.this); //Create new TextView with the settings below
                textView.setTextColor(Color.WHITE);
                textView.setTextSize(15);
                textView.setPadding(50,20,20,20);
                return textView;
            }
        });
        textSwitcher.setText(dialog[stringIndex]); //Start Text
        //Music
        playboy.play(this, R.raw.first);
        playboy.loop(true);
        /*
        if(player1 == null){
            player1 = MediaPlayer.create(this, R.raw.song1);
            player1.start();
        }
         */
        //Go Home:
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button1.setVisibility(View.INVISIBLE);
                button2.setVisibility(View.INVISIBLE);
                nextButton.setVisibility(View.VISIBLE);
                textSwitcher.setText("You: Yeah that's a good idea! I wanna get off the luggage..");
                goHome = true;
            }
        });
        //Go To: RestaurantActivity
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //MusicPlayer.stopPlay(player1);
                //player1 = null;
                button1.setVisibility(View.INVISIBLE);
                button2.setVisibility(View.INVISIBLE);
                nextButton.setVisibility(View.VISIBLE);
                textSwitcher.setText("You: I feel pretty good but I'm hungry... can we go eat somewhere?");
            }
        });
    }

    public void screenPopup(){
        if(stringIndex == 11){
            imageView.animate().alpha(1).setDuration(1000);
        }
        if(stringIndex == 19){
            imageView.setImageResource(R.drawable.aiko_front_shout);
        }
        if(stringIndex == 21){
            imageView.setImageResource(R.drawable.aiko_winter_smile);
            nextButton.setVisibility(View.INVISIBLE);
            button1.setVisibility(View.VISIBLE);
            button2.setVisibility(View.VISIBLE);
        }
    }
}
