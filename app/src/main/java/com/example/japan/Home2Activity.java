package com.example.japan;

import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.example.japan.Helper.BaseActivity;
import com.example.japan.Helper.MusicPlayer;

public class Home2Activity extends BaseActivity {

    private ConstraintLayout layout; //For changing background image later
    final Handler handler = new Handler(); //For delayed execute

    private TextSwitcher textSwitcher; //Like TextView but with Fade in - Fade out for TextView
    private ImageButton nextButton;

    private int stringIndex = 0;
    private String[] dialog = {
            "Aiko's House",
            "You: Phew finally at home! I had so much fun today!",
            "Aiko: Yeah I also had a lot of fun with you today, " + LoginActivity.username,
            "You: I had so much fun! It's always nice hanging out with you!",
            "Aiko: Yup! We could talk and eat a lot! Thanks " + LoginActivity.username,
            "You: Yeah thanks too! You are a wonderful guide and host!",
            "Aiko: Haha thank you ♥",
            "You: Okay! Guess I will take a shower and then go to my room...",
            "Aiko: Good night, " + LoginActivity.username,
            "You: She is really sweet...",
            "You: Okay let's go to my room!", //Case 10
            "You: I dunno but I started to like her a lot the more time we spend together",
            "You: I just feel sorry to Miki, she was also so nice and even gave me her lunch...",
            "You: Dammit what should I do?!",
            "You: God.. it's getting dark...", //Case 14
            "You: I should try to sleep...",
            "You: ....",
            "You: .........",
            "You: ..................",
            "You: Too many things in my head..",
            "You: Maybe I should go outside a bit...",
            "You: Phew...",
            "You: What a nice night sky... So calm and relaxing... I need to get some fresh air\nand empty my head...",
            "Mysterious guy: Hey you!!",
            "You: Huh?",
            "Mysterious guy: Are you the guy who stays at Aiko's home?!", //Case 25
            "You: Who the hell are you?",
            "Mysterious guy: Dude shut up!! I warn you don't piss me off man",
            "You: What are you talking about? Do you know Aiko??",
            "Mysterious guy: It's none of your business... Why don't you leave that place?",
            "You: Huh?",
            "Mysterious guy: You think just because you come to Japan you can stay at her house?",
            "Mysterious guy: Pathetic! I'll show you a lesson now!", //Case 32
            "You: Woah! Calm down!!",
            "Mysterious guy: うるさい！(Shut up)", //Case 34
            //If player shout for help:
            "You: HEEEEEEELP",
            "Mysterious guy: Fuck you!",
            "Aiko: " + LoginActivity.username + "!!!! Are you okay? What happened?", //Case 37
            "You: I am fine thanks... there was a weird guy who seems to know you...",
            "Aiko: ?! Takuya....",
            "You: Who is he?", //Index 40
            "Aiko: Sorry " + LoginActivity.username + "....",
            "You: Are you ok?",
            "Aiko: He was my ex-boyfriend.. we broke up recently and he is still stalking me...",
            "You: Omg... that's so crazy...",
            "Aiko: Yeah... He must be jealous seeing me with you..\nI didn't want you to be involved in this story",
            "You: No no it's okay.. I mean it's not your fault..",
            "Aiko: Thanks... Hey " + LoginActivity.username + ", can I ask you for something?",
            "You: Sure what is it?",
            "Aiko: I don't know if it is safe to stay at my place... maybe you should stay somewhere else..\nI'm sorry..", //Case 49
            //If players says he will protect Aiko:
            "Aiko: But... I can't put you in danger!!",
            "You: Please don't worry! Just trust me!",
            "Aiko: " + LoginActivity.username + "... Thank you ♥",
            "You: Alright let's go back to sleep.. it's late...",
            "Aiko: Yeah!"
    };

    private String[] dialog2 = {
            "Aiko's House",
            "You: Phew finally at home! I had so much fun today!",
            "Aiko: Hey " + LoginActivity.username + " where have you been all day?",
            "You: Oh hey Aiko! Sorry for coming so late...",
            "Aiko: It's okay, I was just worried...",
            "You: Don't worry please! I was just exploring Tokyo a bit.. everything is new for me!",
            "Aiko: Ah I see.. I could have joined you!",
            "You: So sorry! Let's do something together again!",
            "Aiko: Sure.. good night!",
            "You: Damn she is mad...",
            "You: I should go back to my room..", //Case 10
            "You: Aiko is really sweet and a good host, but Miki is also so sweet",
            "You: Especially today, she told me a lot about Asakusa and I could see how happy she was",
            "You: Ahhhh what should I do.. I feel like being in a Harem Anime lol",
            "You: Oh it's already dark...", //Case 14
            "You: I should try to sleep...",
            "You: ....",
            "You: .........",
            "You: ..................",
            "You: Too many things in my head..",
            "You: Maybe go outside a bit...",
            "You: Phew...", //Case 21
            "You: What a nice night sky... So calm and relaxing... I need to get some fresh air\nand empty my head...",
            "Mysterious guy: Hey you!!",
            "You: Huh?",
            "Mysterious guy: Are you the guy who stays at Aiko's home?!", //Case 25
            "You: Who the hell are you?",
            "Mysterious guy: Dude shut up!! I warn you don't piss me off man",
            "You: What are you talking about? Do you know Aiko??",
            "Mysterious guy: It's none of your business... Why don't you leave that place?",
            "You: Huh?",
            "Mysterious guy: You think just because you come to Japan you can stay at her house?",
            "Mysterious guy: Pathetic! I'll show you a lesson now!", //Case 32
            "You: Woah! Calm down!!",
            "Mysterious guy: うるさい！(Shut up)", //Case 34
            //If player shout for help
            "You: HEEEEEEELP!!!!!",
            "Mysterious guy: Fuck you!",
            "Aiko: " + LoginActivity.username + "!!!! Are you okay? What happened?", //Case 37
            "You: I am fine thanks... there was a weird guy who seems to know you...",
            "Aiko: ?! Takuya....",
            "You: Who is he?", //Index 40
            "Aiko: Sorry " + LoginActivity.username + "....",
            "You: Are you ok?",
            "Aiko: He was my ex-boyfriend.. we broke up recently and he is still stalking me...",
            "You: Omg... that's so crazy...",
            "Aiko: Yeah... He must be jealous seeing me with you.. I didn't want you to be involved in this story..",
            "You: No no it's okay.. I mean it's not your fault..",
            "Aiko: Thanks... Hey " + LoginActivity.username + ", can I ask you for something?",
            "You: Sure what is it?",
            "Aiko: I don't know if it is safe to stay at my place... maybe you should stay somewhere else.. I'm sorry", //Case 49
            //If player says he will protect Aiko:
            "Aiko: But... I can't put you in danger!!",
            "You: Please don't worry! Just trust me!",
            "Aiko: " + LoginActivity.username + "... Thank you ♥",
            "You: Alright let's go back to sleep.. it's late...",
            "Aiko: Yeah!"

    };
    private int stringIndex3 = -1;
    private String[] dialog3 = {
            //If player chose to fight:
            "Mysterious guy: Come at me!!",
            "You: I don't even know you, bastard!",
            "Mysterious guy: Fuck off! TAKE THI---",
            "Aiko: STOP!!!!!!!!!!",
            "Mysterious guy: ?!",
            "Aiko: What are you doing here, Takuya???",
            "Takuya: This dude is pissing me off!! Why is he staying at your house?",
            "Aiko: It's none of your business!! Get lost or I call the police!",
            "Takuya: Pff... whatever, you should not trust a guy like him!",
            "Aiko: It's my decision and I don't wanna see you again!!",
            "Takuya: What is so special about this Gaijin?",
            "Aiko: You are an idiot!",
            "Takuya: Fine I go!! Cya~",
            "You: Who was this guy?",
    };
    //If player is unsure where to stay
    private int stringIndex4 = -1;
    private String[] dialog4 = {
            "Aiko: You are right... Maybe we should keep a distance...",
            "You: Aiko...",
            "Aiko: Let's go back to the house.. it's late..",
            "You: Yeah..."
    };

    private TextView textView;
    private ImageView imageView;
    private ImageView roomView;
    private ImageView guyView;
    private ImageView moodView;

    private Button button;
    private Button button2;
    private Button button3;
    private Button button4;
    private ImageButton nextActivity;

    private boolean fight = false;
    private boolean protectAiko = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home2);

        layout = findViewById(R.id.background);

        imageView = findViewById(R.id.imageAiko);
        roomView = findViewById(R.id.roomEvening);
        guyView = findViewById(R.id.imageGuy);
        moodView = findViewById(R.id.imageMood);

        button = findViewById(R.id.button);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        button4 = findViewById(R.id.button4);
        nextActivity = findViewById(R.id.nextButton);

        //Dialog2 if Player chose to spend time with Miki
        if(MensaActivity.spendTimeMiki){
            dialog = dialog2;
            imageView.setImageResource(R.drawable.aiko_summer_frown);
        }

        textSwitcher = findViewById(R.id.textSwitcher);
        nextButton = findViewById(R.id.imageButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){

                if(!protectAiko){
                    dialog = dialog4;
                    fight = false; //reset flag
                    protectAiko = true; //reset flag
                    stringIndex = stringIndex4;
                }

                //Dialog 3 if fight
                if(fight){
                    if(stringIndex3 == dialog3.length-1){
                        fight = false; //reset flag
                        stringIndex = 41;
                        textSwitcher.setText(dialog[stringIndex]);
                    } else {
                        textSwitcher.setText(dialog3[++stringIndex3]); //Else next Text from String
                    }
                } //Otherwise continue dialog1/2
                else if (stringIndex == dialog.length-1){
                    playboy.stop();
                    playboy.play(Home2Activity.this,R.raw.goodnight);
                    nextButton.setVisibility(View.INVISIBLE);
                    textSwitcher.setVisibility(View.INVISIBLE);
                    imageView.setVisibility(View.INVISIBLE);
                    moodView.setVisibility(View.INVISIBLE);
                    roomView.animate().alpha(0).setDuration(1000);
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            roomView.setImageResource(R.drawable.room_morning);
                            roomView.animate().alpha(1).setDuration(3000);
                            nextActivity.setVisibility(View.VISIBLE);
                        }
                    }, 2000);
                } else {
                    textSwitcher.setText(dialog[++stringIndex]); //Else next Text from String
                }
                screenPopup();
            }
        });
        //Fade in - Fade out for text in the textView
        textSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                textView = new TextView(Home2Activity.this); //Create new TextView with the settings below
                textView.setTextColor(Color.WHITE);
                textView.setTextSize(15);
                textView.setPadding(50,20,20,20);
                return textView;
            }
        });
        textSwitcher.setText(dialog[stringIndex]); //Start Text
        //Music
        playboy.play(this, R.raw.home);
        playboy.loop(true);
        //Fight the guy
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                button3.setVisibility(View.INVISIBLE);
                button4.setVisibility(View.INVISIBLE);
                textSwitcher.setText("Mysterious guy: Alright, let's get it on!!");
                fight = true;
            }
        });
        //Shout for help
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                button3.setVisibility(View.INVISIBLE);
                button4.setVisibility(View.INVISIBLE);
                textSwitcher.setText("Mysterious guy: Tch.. What a coward...");
            }
        });
        //Dunno where to go
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                button.setVisibility(View.INVISIBLE);
                button2.setVisibility(View.INVISIBLE);
                textSwitcher.setText("You: Hmmm but where should I go?");
                protectAiko = false;
            }
        });
        //Encourage Aiko
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                button.setVisibility(View.INVISIBLE);
                button2.setVisibility(View.INVISIBLE);
                textSwitcher.setText("You: Hey it's okay! Don't be afraid!! I'll protect you!");
                imageView.setImageResource(R.drawable.aiko_summer_shout_blush);
            }
        });

        nextActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playboy.stop();
                Intent intent = new Intent(Home2Activity.this, School2Activity.class);// New activity
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish(); // Call once you redirect to another activity
            }
        });

    }

    public void screenPopup(){

        if(!fight && protectAiko) {
            switch (stringIndex){
                case 2:
                    imageView.animate().alpha(1).setDuration(1000);
                    break;
                case 8:
                    imageView.animate().translationXBy(1500).setDuration(1000);
                    break;
                case 10:
                    roomView.animate().alpha(1).setDuration(1000);
                    break;
                case 14:
                    layout.setBackgroundResource(R.drawable.room_night);
                    roomView.animate().alpha(0).setDuration(1000);
                    playboy.stop();
                    break;
                case 21:
                    roomView.setImageResource(R.drawable.outside_night);
                    roomView.animate().alpha(1).setDuration(1000);
                    break;
                case 25:
                    playboy.play(Home2Activity.this,R.raw.dangerous);
                    playboy.loop(true);
                    guyView.animate().translationX(0).setDuration(1000);
                    break;
                case 32:
                    guyView.setImageResource(R.drawable.guy2);
                    break;
                case 34:
                    nextButton.setVisibility(View.INVISIBLE);
                    button3.setVisibility(View.VISIBLE);
                    button4.setVisibility(View.VISIBLE);
                    break;
                case 35:
                    guyView.setImageResource(R.drawable.guy3);
                    break;
                case 36:
                    guyView.animate().translationX(-1300).setDuration(1000);
                    break;
                case 37:
                    imageView.setImageResource(R.drawable.aiko_summer_frown);
                    imageView.animate().translationX(0).setDuration(1000);
                    break;
                case 39:
                    imageView.setImageResource(R.drawable.aiko_summer_shout);
                    break;
                case 41:
                    imageView.setImageResource(R.drawable.aiko_summer_frown);
                    moodView.setImageResource(R.drawable.tear);
                    moodView.setVisibility(View.VISIBLE);
                    break;
                case 43:
                    moodView.setVisibility(View.INVISIBLE);
                    break;
                case 49:
                    nextButton.setVisibility(View.INVISIBLE);
                    button.setVisibility(View.VISIBLE);
                    button2.setVisibility(View.VISIBLE);
                    break;
                case 52:
                    moodView.setImageResource(R.drawable.heart);
                    moodView.setVisibility(View.VISIBLE);
                    break;
            }
        } else { //If Players choose FIGHT
            switch(stringIndex3){
                case 5:
                    imageView.setImageResource(R.drawable.aiko_summer_shout);
                    imageView.animate().translationX(0).setDuration(1000);
                    break;
                case 8:
                    guyView.setImageResource(R.drawable.guy3);
                    break;
                case 11:
                    moodView.setVisibility(View.VISIBLE);
                    break;
                case 13:
                    moodView.setVisibility(View.INVISIBLE);
                    guyView.animate().translationX(-1300).setDuration(1000);
                    break;
            }
        }

    }
}
