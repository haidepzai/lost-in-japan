package com.example.japan;


import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.example.japan.Helper.BaseActivity;

public class HomeActivity extends BaseActivity {

    private ConstraintLayout layout;

    //visited restaurant at beginning (switched to true if player choose to go to restaurant at beginning)
    public static boolean visitedRestaurant = false; //Is set true in MainActivity if player choose to go there
    public static boolean comeBackHome = false;
    //come back to home for the 2nd time when have not visited the restaurant at the beginning
    // (switched to true if player come here first and go to the restaurant)

    private TextSwitcher textSwitcher; //Like TextView but with Fade in - Fade out for TextView
    private ImageButton nextButton;

    private int stringIndex = 0;
    private String[] dialog = {
            "Aiko's House",
            "Aiko: So this is my house! Feel yourself like home!",
            "You: Thank you for your kindness and hospitality, Aiko",
            "Aiko: Do you want to have some tea? Have you ever tried Japanese green tea yet?",
            "You: Not yet, I would like to!!",
            "Aiko: Okay! Wait a moment please!",
            "You: Sure!",
            "You: (Wow this must be a dream... It's so nice to live like a local!! AWESOME!)",
            "You: (... I have a feeling that this will be the best time in my life! Uh yeah \uD83D\uDE01 )",
            "Aiko: Okay! Here you go!", //Case: 9
            "You: ありがとうございます (Thank you so much)!",
            "Aiko: Ahahaha not bad!! But you should improve your pronunciation!",
            "You: Yeah I should... I hope to improve my Japanese while I am here",
            "Aiko: You can do it!!!!", //Index 13
            //After come back home
            "Aiko: Okay, so how about you take a shower and a bath and afterwards, I'll show you the room?",
            "You: Sounds great!",
            "Aiko: See you later!", //Case 16
            "You: (She is soooo kind wow!)",
            "*after you have finished your bath you returned into your room*", //Case 18
            "You: Phew that was a nice bath!! So hot and relaxing, I can get use to it!!",
            "You: And what a nice room! I am so happy to have met such a nice girl like Aiko!",
            "You: I guess it's time to get some sleep...",
            "You: Tomorrow will be a good day!",
            "\uD83D\uDCA4 \uD83D\uDCA4 \uD83D\uDCA4",
            "ZzzZZZZzzzZzzz",
            "You: Ohhhh it's already morning! What a nice weather \uD83D\uDE04",
            "Aiko: Good Morning, " + LoginActivity.username + " ! Did you sleep well?", //Case 26
            "You: Yeah I do!! This room is so comfy and nice... I could sleep very well!!",
            "Aiko: I am so glad to hear! I was a bit worried but felt relieved!",
            "You: I cannot thank you enough, Aiko!!",
            "Aiko: You are really a nice guy, " + LoginActivity.username,
            "You: Hehe you too! Okay I have to go to my school now! Will be a new experience for me!",
            "Aiko: Have a great day!"
    };
    private int stringIndex2 = 0;
    private String[] dialog2 = { //Appears when User did not go to restaurant at the beginning
            "Aiko: You must be hungry! How about we will go to a restaurant together?",
            "You: That sounds great! I'm dying!!",
            "Aiko: Okay I know a good restaurant nearby! Let's go!",
            "You: Yeaaaah let's gooooo!!"
    };
    private String[] dialog3 = {
            "Aiko: Oh.. I am sorry for that..",
            "You: Ahhh it's ok! I will get use to it!!",
            "Aiko: ...",
            "You: Alright! I have to go to school now",
            "Aiko: Hope you have lot of fun! Have a nice day!"
    };

    private TextView textView;
    private ImageView imageView;
    private ImageView roomView;
    private ImageView moodView;
    private ImageView teaView;

    private Button button1;
    private Button button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        //To continue the dialog from index 13 after come back home
        layout = findViewById(R.id.background);

        button1 = findViewById(R.id.button);
        button2 = findViewById(R.id.button2);

        imageView = findViewById(R.id.imageAiko);
        roomView = findViewById(R.id.roomEvening);
        moodView = findViewById(R.id.imageMood);
        teaView = findViewById(R.id.imageTea);

        if(comeBackHome){
            imageView.animate().alpha(1);
            stringIndex = 14;
            comeBackHome = false;
        }

        textSwitcher = findViewById(R.id.textSwitcher);
        nextButton = findViewById(R.id.imageButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Log.i("Info", String.valueOf(stringIndex));
                Log.i("Info", String.valueOf(visitedRestaurant));
                //Dialog 2 if not have visited Restaurant before and dialog 1 = 12th index
                //Went home first
                if(!visitedRestaurant && stringIndex == 13){
                    Log.i("Info", "Rein");
                        if (stringIndex2 == dialog2.length - 1) {
                            playboy.stop();
                            Intent intent = new Intent(HomeActivity.this, RestaurantActivity.class);// New activity
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT); //Continue Activity after come back home
                            startActivity(intent);
                        } else {
                            textSwitcher.setText(dialog2[stringIndex2]);
                            stringIndex2++;
                    }
                } //Standard Dialog 1 (First restaurant then home)
                else if (stringIndex == dialog.length-1){
                    //stringIndex = 0;
                    //textSwitcher.setText(dialog[stringIndex]); //if stringIndex at the End : stringIndex = 0 : Start Text from Beginning
                    playboy.stop();
                    Intent intent = new Intent(HomeActivity.this, SchoolActivity.class);// New activity
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish(); // Call once you redirect to another activity
                } else {
                    textSwitcher.setText(dialog[++stringIndex]); //Else next Text from String
                }
                screenPopup();
            }
        });
        //Fade in - Fade out for text in the textView
        textSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                textView = new TextView(HomeActivity.this); //Create new TextView with the settings below
                textView.setTextColor(Color.WHITE);
                textView.setTextSize(15);
                textView.setPadding(50,20,20,20);
                return textView;
            }
        });
        textSwitcher.setText(dialog[stringIndex]); //Start Text
        //Music
        playboy.play(this, R.raw.home);
        playboy.loop(true);
        //Slept well
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                stringIndex = 27;
                textSwitcher.setText(dialog[stringIndex]);
                button1.setVisibility(View.INVISIBLE);
                button2.setVisibility(View.INVISIBLE);
            }
        });
        //Didn't sleep well
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setVisibility(View.VISIBLE);
                textSwitcher.setText(dialog3[0]);
                dialog = dialog3;
                stringIndex = 0;
                button1.setVisibility(View.INVISIBLE);
                button2.setVisibility(View.INVISIBLE);
                imageView.setImageResource(R.drawable.aiko_front_frow);
                moodView.setVisibility(View.VISIBLE);
            }
        });
    }

    public void screenPopup(){
        switch(stringIndex){
            case 1:
                imageView.animate().alpha(1).setDuration(1000);
                break;
            case 6:
            case 16:
                imageView.animate().translationX(1500).setDuration(1000);
                break;
            case 9:
                imageView.animate().translationX(0).setDuration(1000);
                teaView.setVisibility(View.VISIBLE);
                teaView.animate().scaleX(1).scaleY(1).setDuration(1000);
                break;
            case 10:
                teaView.setVisibility(View.INVISIBLE);
            case 11:
                imageView.setImageResource(R.drawable.aiko_front_shout);
                break;
            case 13:
                imageView.setImageResource(R.drawable.aiko_front_smile);
                break;
            case 18:
                roomView.animate().alpha(1).setDuration(1000);
                break;
            case 22:
                playboy.stop();
                break;
            case 23:
                playboy.play(HomeActivity.this,R.raw.goodnight);
                roomView.setImageResource(R.drawable.room_night);
                break;
            case 24:
                layout.setBackgroundResource(R.drawable.room_morning);
                roomView.animate().alpha(0).setDuration(3000);
                break;
            case 26:
                imageView.setImageResource(R.drawable.aiko_winter_smile);
                imageView.animate().translationX(0).setDuration(1000);
                nextButton.setVisibility(View.INVISIBLE);
                button1.setVisibility(View.VISIBLE);
                button2.setVisibility(View.VISIBLE);
                break;
            case 28:
                imageView.setImageResource(R.drawable.aiko_front_shout_blush);
                moodView.setImageResource(R.drawable.heart);
                moodView.setVisibility(View.VISIBLE);
        }

    }
}
